-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 15, 2020 at 12:30 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ashtry`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
CREATE TABLE IF NOT EXISTS `carts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `order_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `carts_user_id_foreign` (`user_id`),
  KEY `carts_product_id_foreign` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `imagable_id` int(11) DEFAULT NULL,
  `imagable_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `imagable_id`, `imagable_type`, `name`, `img_url`, `created_at`, `updated_at`) VALUES
(12, 1, 'setting', '2020/01/1578997416_hhhhhhhhh.png', NULL, '2020-01-14 08:23:36', '2020-01-14 08:23:36'),
(13, 2, 'setting', '2020/01/1579000312_من نحن @2x.png', NULL, '2020-01-14 09:11:52', '2020-01-14 09:11:52'),
(14, 3, 'setting', '2020/01/1579000403_الهدف.jpeg', NULL, '2020-01-14 09:13:23', '2020-01-14 09:13:23'),
(15, 4, 'setting', '2020/01/1579000505_ro2ya.png', NULL, '2020-01-14 09:15:05', '2020-01-14 09:15:05'),
(16, 5, 'setting', '2020/01/1579034741_ro2ya.png', NULL, '2020-01-14 18:45:41', '2020-01-14 18:45:41');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type_message` int(11) NOT NULL DEFAULT 0,
  `read_notifiy` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message`, `user_id`, `type_message`, `read_notifiy`, `created_at`, `updated_at`) VALUES
(16, 'knoioi', 7, 1, 0, NULL, NULL),
(17, 'السلام عليكم استاذ أحمد', 7, 0, 0, '2020-01-14 21:22:45', '2020-01-14 21:22:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=195 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(178, '2014_10_12_000000_create_users_table', 1),
(179, '2014_10_12_100000_create_password_resets_table', 1),
(180, '2016_01_15_105324_create_roles_table', 1),
(181, '2016_01_15_114412_create_role_user_table', 1),
(182, '2016_01_26_115212_create_permissions_table', 1),
(183, '2016_01_26_115523_create_permission_role_table', 1),
(184, '2016_02_09_132439_create_permission_user_table', 1),
(185, '2019_08_19_000000_create_failed_jobs_table', 1),
(186, '2020_01_04_150747_create_orders_table', 1),
(187, '2020_01_04_183842_create_images_table', 1),
(188, '2020_01_05_104928_create_shipping_infos_table', 1),
(189, '2020_01_05_153514_create_products_table', 1),
(190, '2020_01_06_001827_create_carts_table', 1),
(191, '2020_01_06_134734_create_sessions_table', 1),
(192, '2020_01_06_201908_create_purchasings_table', 1),
(193, '2020_01_13_125128_create_messages_table', 2),
(194, '2020_01_14_064059_create_settings_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notise` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `product_name`, `product_link`, `notise`, `quantity`, `size`, `color`, `order_status`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, 'http://localhost:8000/admin/orders-buy', 'ملاحظات ملاحظات ملاحظات ملاحظات ملاحظات ملاحظات ملاحظات ملاحظات', 12, 'XL', 'احمر', 1, '2020-01-11 19:30:46', '2020-01-12 21:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `model`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Can View Users', 'view.users', 'Can view users', 'Permission', '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL),
(2, 'Can Create Users', 'create.users', 'Can create new users', 'Permission', '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL),
(3, 'Can Edit Users', 'edit.users', 'Can edit users', 'Permission', '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL),
(4, 'Can Delete Users', 'delete.users', 'Can delete users', 'Permission', '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL),
(2, 2, 1, '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL),
(3, 3, 1, '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL),
(4, 4, 1, '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
CREATE TABLE IF NOT EXISTS `permission_user` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL DEFAULT 0,
  `discount` int(11) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `discount`, `details`, `created_at`, `updated_at`) VALUES
(19, 'product 0', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(2, 'product 1', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(3, 'product 2', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(4, 'product 3', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(5, 'product 4', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(6, 'product 5', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(7, 'product 6', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(8, 'product 7', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(9, 'product 8', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(10, 'product 9', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(11, 'product 10', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-10 22:46:48', '2020-01-10 22:46:48'),
(12, 'منتج جديد 1', 100, 10, 'منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 ر منتج جديد 1 منتج جديد 1', '2020-01-12 13:03:59', '2020-01-12 13:03:59'),
(13, 'منتج جديد 1 -1', 120, 12, 'منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 منتج جديد 1 ر منتج جديد 1 منتج جديد 1  12', '2020-01-12 13:05:47', '2020-01-13 10:22:21'),
(14, 'منتج جديد رقم 2', 100, 5, 'تفاصيل منتج جديد رقم 2 منتج جديد رقم 2 تفاصيل منتج جديد رقم 2 منتج جديد رقم 2 تفاصيل منتج جديد رقم 2 منتج جديد رقم 2 تفاصيل منتج جديد رقم 2 منتج جديد رقم 2 تفاصيل منتج جديد رقم 2 منتج جديد رقم 2', '2020-01-12 13:08:03', '2020-01-12 13:08:03'),
(15, 'منتج جديد 3 4', 1200, 12, 'تفاصيل منتج جديد 3 منتج جديد 3 منتج جديد 3 تفاصيل منتج جديد 3 منتج جديد 3 منتج جديد 3  تفاصيل منتج جديد 3 منتج جديد 3 منتج جديد 3  تفاصيل منتج جديد 3 منتج جديد 3 منتج جديد 3  تفاصيل منتج جديد 3 منتج جديد 3 منتج جديد 2', '2020-01-12 13:10:18', '2020-01-13 10:40:15'),
(16, 'منتج جديد رقم 4', 123, 12, 'تفاصيل منتج جديد رقم 4 تفاصيل منتج جديد رقم 4 تفاصيل منتج جديد رقم 4 تفاصيل منتج جديد رقم 4 ر ر تفاصيل منتج جديد رقم 4', '2020-01-12 13:12:07', '2020-01-12 13:12:07'),
(17, 'منتج جديد رقم 5', 45, 10, 'تفاصيل تفاصيل منتج جديد رقم 5 منتج جديد رقم 5 منتج جديد رقم 5', '2020-01-12 13:13:08', '2020-01-12 13:13:08'),
(20, 'product 1', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(21, 'product 2', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(22, 'product 3', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(23, 'product 4', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(24, 'product 5', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(25, 'product 6', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(26, 'product 7', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(27, 'product 8', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(28, 'product 9', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(29, 'product 10', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:11:56', '2020-01-14 07:11:56'),
(30, 'product 0', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(31, 'product 1', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(32, 'product 2', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(33, 'product 3', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(34, 'product 4', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(35, 'product 5', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(36, 'product 6', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(37, 'product 7', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(38, 'product 8', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(39, 'product 9', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(40, 'product 10', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:13:02', '2020-01-14 07:13:02'),
(41, 'product 0', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(42, 'product 1', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(43, 'product 2', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(44, 'product 3', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(45, 'product 4', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(46, 'product 5', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(47, 'product 6', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(48, 'product 7', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(49, 'product 8', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(50, 'product 9', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46'),
(51, 'product 10', 120, 10, 'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف \n	        	           المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج', '2020-01-14 07:14:46', '2020-01-14 07:14:46');

-- --------------------------------------------------------

--
-- Table structure for table `purchasings`
--

DROP TABLE IF EXISTS `purchasings`;
CREATE TABLE IF NOT EXISTS `purchasings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_ids` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `quantities` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchasings_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin', 'Admin Role', 5, '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL),
(2, 'User', 'user', 'User Role', 1, '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL),
(3, 'Unverified', 'unverified', 'Unverified Role', 0, '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL),
(2, 2, 2, '2020-01-10 22:44:23', '2020-01-10 22:44:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_setting` int(11) NOT NULL DEFAULT 0,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `type_setting`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'الواجهة', 'مرحبا بك في اشتري', '2020-01-14 07:14:46', '2020-01-14 19:48:45'),
(2, 2, 'من نحن', '\"اشتري\"\r\nموقع إلكتروني يهدف إلى خدمة عشاق التسوق من أنحاء العالم كافةً من خلال تقديم خِدْمات تُقدِّم لك عروضًا شرائية بشكل يسير وواضح.', '2020-01-14 07:14:46', '2020-01-14 09:11:52'),
(3, 3, 'هدفنا', 'انطلاقًا من رؤية موقع \"اشتري\" ورسالته الواضحة نهدف إلى تقديم خِدْمات: التسوق والشراء بالنيابة عن عملائنا والمحافظة على رضاهم.', '2020-01-14 07:14:46', '2020-01-14 09:13:23'),
(4, 4, 'رؤيتنا', 'نسعى إلى تطوير متجرنا ومنصتنا الإلكترونية لنصبح الهدف الأول لعشاق التسوق كافةً في الوطن العربي أجمع في مجال: التسوق والشراء بالنيابة من جميع المتاجر الإلكترونية.', '2020-01-14 07:14:46', '2020-01-14 09:15:05');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_infos`
--

DROP TABLE IF EXISTS `shipping_infos`;
CREATE TABLE IF NOT EXISTS `shipping_infos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `user_last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_phone_number` int(11) NOT NULL,
  `user_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_region` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_post_code` int(11) NOT NULL,
  `user_street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_infos_user_id_foreign` (`user_id`),
  KEY `shipping_infos_order_id_foreign` (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$Ff1uuguYVypK/qQTM1w32.oIl8xO/HIX9575ihynxH6k5H197w2yy', NULL, '2020-01-10 22:44:23', '2020-01-10 22:44:23'),
(2, 'User', 'user@user.com', NULL, '$2y$10$FMq23xo6WSwJYJECEQtWDOhmFMWH6Q0IFbd1RidGsvxhJaOSW7uHq', NULL, '2020-02-10 22:44:23', '2020-01-10 22:44:23'),
(5, 'xcx', 'xcx@gmail.com', NULL, 'password', NULL, '2020-02-11 23:08:19', NULL),
(6, 'mohamed', 'mohamed@moahmed.com', NULL, 'password', NULL, '2020-03-21 00:17:06', NULL),
(7, 'ahmed', 'ahmed@ahmed@gmail.com', NULL, '', NULL, '2020-04-28 02:08:12', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
