<?php

use Illuminate\Database\Seeder;
use App\models\setting;
class addSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$SettingItems = [
            [
	        	'type_setting'    =>'1',
	        	'title'           =>'Front',
	        	'content'         =>'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف 
	        	                     المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج',
	        ],
	         [
	        	'type_setting'    =>'2 ',
	        	'title'           =>'about_us',
	        	'content'         =>'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف 
	        	                     المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج',
	        ],
	         [
	        	'type_setting'    =>'3 ',
	        	'title'           =>'vission',
	        	'content'         =>'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف 
	        	                     المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج',
	        ],
	         [
	        	'type_setting'    =>'4 ',
	        	'title'           =>'mission',
	        	'content'         =>'وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج وصف 
	        	                     المنتج وصف المنتج وصف المنتج وصف المنتج وصف المنتج',
	        ]
        ];

        foreach ($SettingItems as $item) {
	    	    setting::create([
		        	'type_setting'    =>$item['type_setting'],
		        	'title'           =>$item['title'],
		        	'content'         =>$item['content'],
	    	    ]);	
        }

    }
}
