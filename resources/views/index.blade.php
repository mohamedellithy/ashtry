@extends('user.layouts.index')
@section('title')
   المشترى
@endsection

@section('content-page')
   <div class="tab-pane active col-12" id="container-content-page" role="tabpanel">    	
       @include('user.pages.search-and-purchasing')
       
       
   </div>
  
@endsection