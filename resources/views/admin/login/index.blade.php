<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <title> admin-login </title>

        @include('admin.layouts.includes.header')
    </head>


    <body class="fixed-left">

        <div class="wrapper-page">

            <div class="text-center">
                <a href="index.html" class="logo-lg" style="font-family: cairo;"><i class="mdi mdi-radar"></i> <span> اشترى </span> </a>
            </div>

            
            <form class="form-horizontal m-t-20" method="POST" action="{{ route('login') }}">
              @csrf
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="mdi mdi-account"></i></span>
                        </div>
                        <input type="email" placeholder="البريد الالكترونى" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="mdi mdi-key"></i></span>
                            </div>
                            <input type="password" placeholder="كلمة المرور" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
        
                <div class="form-group text-left">
                    <div class="checkbox checkbox-primary">
                        
                        <input id="checkbox-signup" type="checkbox" class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="checkbox-signup">
                            تذكرنى
                        </label>
                    </div>
                </div>

                <div class="form-group text-right m-t-20">
                    <button class="btn btn-primary btn-custom w-md waves-effect waves-light" type="submit"> تسجيل الدخول </button>
                </div>

              
            </form>
        </div>
       
        @include('admin.layouts.includes.scripts')

       
    </body>
</html>
