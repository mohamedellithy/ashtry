    
    <script>
        var resizefunc = [];
    </script>

    <!-- Plugins  -->

    <script src="{{ asset('assets/admin/js/popper.min.js') }} "></script><!-- Popper for Bootstrap -->
    <script src="{{ asset('assets/admin/js/bootstrap.min.js') }} "></script>



    <!-- Counter Up  -->
    <script src="{{ asset('assets/admin/plugins/waypoints/lib/jquery.waypoints.min.js') }} "></script>
    <script src="{{ asset('assets/admin/plugins/counterup/jquery.counterup.min.js') }} "></script>

    <!-- circliful Chart -->
    <script src="{{ asset('assets/admin/plugins/jquery-circliful/js/jquery.circliful.min.js') }} "></script>
    <script src="{{ asset('assets/admin/plugins/jquery-sparkline/jquery.sparkline.min.js') }} "></script>

    <!-- skycons -->
    <script src="{{ asset('assets/admin/plugins/skyicons/skycons.min.js') }} " type="text/javascript"></script>

    @yield('put-datatable-script')
   

    <!-- Page js  -->
    <script src="{{ asset('assets/admin/pages/jquery.dashboard.js') }} "></script>
    

    
    


    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.counter').counterUp({
                delay: 100,
                time: 1200
            });
            $('.circliful-chart').circliful();
        });

        // BEGIN SVG WEATHER ICON
        if (typeof Skycons !== 'undefined'){
            var icons = new Skycons(
                    {"color": "#3bafda"},
                    {"resizeClear": true}
                    ),
                    list  = [
                        "clear-day", "clear-night", "partly-cloudy-day",
                        "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                        "fog"
                    ],
                    i;

            for(i = list.length; i--; )
                icons.set(list[i], list[i]);
            icons.play();
        };


    </script>
    @yield('add-scripts')