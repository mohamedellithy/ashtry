 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo">
 <link href="{{ asset('assets/admin/plugins/switchery/switchery.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/admin/plugins/jquery-circliful/css/jquery.circliful.css') }}" rel="stylesheet" type="text/css" />

<link href="{{asset('assets/admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/admin/css/icons.css') }}" rel="stylesheet" type="text/css">
@yield('put-datatable-css')

<link href="{{asset('assets/admin/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/admin/css/admin_style.css') }}" rel="stylesheet" type="text/css">

<script src="{{asset('assets/admin/js/modernizr.min.js')}}"></script>
<!-- <script src="{{ asset('assets/admin/js/jquery.min.js') }} "></script> -->
<script src="{{ asset('assets/admin/js/jQuery-9.js') }} "></script>

        