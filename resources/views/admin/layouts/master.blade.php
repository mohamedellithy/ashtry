<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <title> @yield('title') </title>


        @include('admin.layouts.includes.header')
        
        <meta name="csrf-token" content="{{ csrf_token() }}" />

    </head>


    <body class="fixed-left">
        
        <!-- Begin page -->
        <div id="wrapper">

           @include('admin.layouts.includes.navbar')

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        
                        @yield('content-admin-page')
                        <!-- Page-Title -->
                       <!--  <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Welcome !</h4>
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Minton</a></li>
                                        <li class="breadcrumb-item active">Dashboard</li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

 -->
                       <!--  <div class="row">
                            <div class="col-sm-6 col-lg-3">
                                <div class="widget-simple-chart text-right card-box">
                                    <div class="circliful-chart" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2"></div>
                                    <h3 class="text-success counter m-t-10">2562</h3>
                                    <p class="text-muted text-nowrap m-b-10">Total Sales today</p>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-3">
                                <div class="widget-simple-chart text-right card-box">
                                    <div class="circliful-chart" data-dimension="90" data-text="75%" data-width="5" data-fontsize="14" data-percent="75" data-fgcolor="#3bafda" data-bgcolor="#ebeff2"></div>
                                    <h3 class="text-primary counter m-t-10">5685</h3>
                                    <p class="text-muted text-nowrap m-b-10">Daily visitors</p>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-3">
                                <div class="widget-simple-chart text-right card-box">
                                    <div class="circliful-chart" data-dimension="90" data-text="58%" data-width="5" data-fontsize="14" data-percent="58" data-fgcolor="#f76397" data-bgcolor="#ebeff2"></div>
                                    <h3 class="text-pink m-t-10">$ <span class="counter">12480</span></h3>
                                    <p class="text-muted text-nowrap m-b-10">Total Earning</p>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-3">
                                <div class="widget-simple-chart text-right card-box">
                                    <div class="circliful-chart" data-dimension="90" data-text="49%" data-width="5" data-fontsize="14" data-percent="49" data-fgcolor="#98a6ad" data-bgcolor="#ebeff2"></div>
                                    <h3 class="text-inverse counter m-t-10">62</h3>
                                    <p class="text-muted text-nowrap m-b-10">Pending Orders</p>
                                </div>
                            </div>
                        </div> -->
                        <!-- end row -->
                    </div>
                    <!-- end container -->
                </div>
                <!-- end content -->
               
                @include('admin.layouts.includes.footer')

            </div>
        </div>
        <!-- END wrapper -->

        @include('admin.layouts.includes.scripts')

        @yield('script-datatable')

        @yield('javascript')
    </body>
       
</html>