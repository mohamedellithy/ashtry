@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('content-admin-page')
       <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box ">
                    <h4 class="page-title"> مرحبا بك !</h4>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">اشترى </a></li>
                        <li class="breadcrumb-item active"> الرئيسية </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-3" >
                <a href="{{ url('admin/orders') }}">
                    
                    <div class="widget-simple-chart text-right card-box bg-warning ">
                         <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-cart"></i></div>   
                        

                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">الطلبات</p>
                        <h3 class="text-success counter m-t-10" style="color:white !important">{{ App\models\orders::count() + App\models\Purchasing::count() }}</h3>
                        <hr style="border-top: 2px solid rgba(255, 253, 48, 1);" />
                        <label class="percentage_value"> 100% </label>
                        <!-- <div class="circliful-chart" style="color:white !important;margin-top: -20px;" data-dimension="90" data-text="100%" data-width="5" data-fontsize="14" data-percent="100" data-fgcolor="rgb(255, 253, 48)" data-bgcolor="#ebeff2">
                        </div> -->
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/activites') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning"   >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-cellphone-link"></i></div>   
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">واجهة المستخدم</p>
                        <h3 class="text-primary counter m-t-10" style="color:white !important">{{ App\models\Product::count() + App\User::count() }}</h3>
                        <hr style="  border-top: 2px solid rgba(255, 253, 48, 1);" />
                         <label class="percentage_value"> 100% </label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/mails') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning text-white" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-email"></i></div>   
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important"> صندوق البريد </p>
                        <h3 class="text-pink m-t-10" style="color:white !important;margin-top: -20px;"> <span class="counter"> {{ App\models\Message::count() }} </span></h3>
                        <hr style="  border-top: 2px solid rgba(255, 253, 48, 1);" />
                           <label class="percentage_value"> 100% </label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/reports') }} ">
                    <div class="widget-simple-chart text-right card-box bg-warning text-white" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class=" mdi mdi-elevation-rise"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important"> التقارير </p>
                        <h3 class="text-inverse counter m-t-10" style="color:white !important;"></h3>
                        <hr style="  border-top: 2px solid rgba(255, 253, 48, 1);" />
                        <label class="percentage_value"> 100% </label>
                    </div>
                </a>
            </div>
        </div>
        <!-- end row -->

        <!-- BAR Chart -->
        <div class="row">
          
            <div class="col-lg-12">
                <div class="container-diagrame">
                    <h3 class="text-right" style="text-align:right !important">{{ $chart1->options['chart_title'] }}</h3>
                    {!! $chart1->renderHtml() !!}
                </div>
            </div>
            <!-- col -->
        </div>
        <!-- End row-->
        <br/>        
        <!-- BAR Chart -->
        <div class="row">
          
            <div class="col-lg-12">
                <div class="container-diagrame">
                    <h3 class="text-right" style="text-align:right !important">{{ $chart2->options['chart_title'] }}</h3>
                    {!! $chart2->renderHtml() !!}
                </div>
            </div>
            <!-- col -->
        </div>
        <!-- End row-->
                

 
@endsection

@section('javascript')
{!! $chart1->renderChartJsLibrary() !!}
{!! $chart1->renderJs() !!}

{!! $chart2->renderChartJsLibrary() !!}
{!! $chart2->renderJs() !!}
@endsection