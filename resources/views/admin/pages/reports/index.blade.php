@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('put-datatable-css')
<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  -->
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('put-datatable-script')
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  -->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection


@section('content-admin-page')
        
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> طلبات الشراء !</h4>
                    <a class="go_back" href="{{ url('admin/orders') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> الطلبات الشراء </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        
        <div class="row">
           
             <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_user1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    {!! $chart_user1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_user_daily1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                  
                    {!! $chart_user_daily1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_user_orders1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_user_orders1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_user_orders_buy1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_user_orders_buy1->renderHtml() !!}
                </div>
            </div>

             <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_user_orders_buy_and_search1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_user_orders_buy_and_search1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_orders_month1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_orders_month1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_orders_day1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_orders_day1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_orders_buy_month1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_orders_buy_month1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_orders_buy_day1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_orders_buy_day1->renderHtml() !!}
                </div>
            </div>


            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_orders_buy_and_search_month1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_orders_buy_and_search_month1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_orders_buy_and_search_day1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_orders_buy_and_search_day1->renderHtml() !!}
                </div>
            </div>


            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_orders_discount_month1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_orders_discount_month1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_orders_discount_day1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_orders_discount_day1->renderHtml() !!}
                </div>
            </div>

            <div class="container_datatable container_datatable_reports col-sm-6">
                <label class="heading-reports"> {{ $chart_user_orders_discounts1->options['chart_title'] }} </label>
                <br/>
                <div class="container-diagrame">
                    
                    {!! $chart_user_orders_discounts1->renderHtml() !!}
                </div>
            </div>



            


        </div>


@endsection

@section('javascript')
{!! $chart_user1->renderChartJsLibrary() !!}
{!! $chart_user1->renderJs() !!}

{!! $chart_user_daily1->renderChartJsLibrary() !!}
{!! $chart_user_daily1->renderJs() !!}

{!! $chart_user_orders1->renderChartJsLibrary() !!}
{!! $chart_user_orders1->renderJs() !!}

{!! $chart_user_orders_buy1->renderChartJsLibrary() !!}
{!! $chart_user_orders_buy1->renderJs() !!}


{!! $chart_user_orders_buy_and_search1->renderChartJsLibrary() !!}
{!! $chart_user_orders_buy_and_search1->renderJs() !!}


{!! $chart_orders_month1->renderChartJsLibrary() !!}
{!! $chart_orders_month1->renderJs() !!}



{!! $chart_orders_day1->renderChartJsLibrary() !!}
{!! $chart_orders_day1->renderJs() !!}


{!! $chart_orders_buy_month1->renderChartJsLibrary() !!}
{!! $chart_orders_buy_month1->renderJs() !!}


{!! $chart_orders_buy_day1->renderChartJsLibrary() !!}
{!! $chart_orders_buy_day1->renderJs() !!}


{!! $chart_orders_buy_and_search_month1->renderChartJsLibrary() !!}
{!! $chart_orders_buy_and_search_month1->renderJs() !!}


{!! $chart_orders_buy_and_search_day1->renderChartJsLibrary() !!}
{!! $chart_orders_buy_and_search_day1->renderJs() !!}


{!! $chart_orders_discount_month1->renderChartJsLibrary() !!}
{!! $chart_orders_discount_month1->renderJs() !!}


{!! $chart_orders_discount_day1->renderChartJsLibrary() !!}
{!! $chart_orders_discount_day1->renderJs() !!}


{!! $chart_user_orders_discounts1->renderChartJsLibrary() !!}
{!! $chart_user_orders_discounts1->renderJs() !!}






@endsection