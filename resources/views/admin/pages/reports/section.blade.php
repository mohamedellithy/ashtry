        <div class="row">
           <div class="col-12">
                <form method="post" action="{{ url('admin/orders/update-status/') }}" >
                   @csrf
                    <div class="card-box">
                        <div class="row">
                                <div class="col-md-4">
                                    <h4 class="m-t-0 header-title"> قسم البحث</h4>
                                    <select name="status_update" class="custom-select mt-3" required>
                                        <option selected> اختر قسم البحث </option>
                                        <option value="0" > كل الطلبات </option>
                                        <option value="0" > طلبات الشراء </option>
                                        <option value="0" > طلبات الشراء و البحث </option>
                                        <option value="0" > طلبات متجر التخفيضات </option>
                                        <option value="0" > المستخدمين </option>
                                        <option value="0" > المنتجات </option>
                                        
                                    </select>
                                   
                                </div>
                                 <div class="col-md-4">
                                    <h4 class="m-t-0 header-title"> البحث عن</h4>
                                    <select name="status_update" class="custom-select mt-3" required>
                                        <option selected> استخراج ك </option> -->
                                        <option value="0" > انتظار </option>
                                        <option value="1" > موافقة على الطلب </option>
                                        <option value="2" > استلام الطلب</option>
                                        <option value="3" > رفض الطلب</option>
                                    </select>
                                   
                                </div>
                                <div class="container-button-cpanel col-md-3">
                                     <button type="submit" class="btn btn-primary"> استخراج التقرير </button>
                                </div>
                            
                        </div>
                    </div> <!-- end card-box -->
                </form>
            </div> <!-- end col -->
        </div>
        <!-- end row -->