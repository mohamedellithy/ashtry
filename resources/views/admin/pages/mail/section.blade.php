        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/mails') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class=" mdi mdi-email"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important"> الرسائل </p>
                        <h3 class="text-success counter m-t-10" style="color:white !important"> {{ App\models\Message::count() }}</h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >%100</label>

                    </div>
                </a>
            </div>

            <!-- <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/mails-recieved') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" style="">
                        <div class="circliful-chart" style="color:white !important" data-dimension="90" data-text="{{ calaulate_percentage_for_circules(App\models\Message::where(['type_message'=>1])->count(), App\models\Message::count() ) }}%" data-width="5" data-fontsize="14" data-percent="{{ calaulate_percentage_for_circules(App\models\Message::where(['type_message'=>1])->count(), App\models\Message::count() ) }}" data-fgcolor="rgb(255, 253, 48)" data-bgcolor="white"></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important"> الرسائل المستلمة</p>
                        <h3 class="text-primary counter m-t-10" style="color:white !important">{{ App\models\Message::where(['type_message'=>1])->count() }}</h3>
                    </div>
                </a>
            </div>
 -->
            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/mails-send') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class=" mdi mdi-email"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important"> رسائل المرسلة </p>
                        <h3 class="text-inverse counter m-t-10" style="color:white !important">{{ App\models\Message::where(['type_message'=>0])->count() }}</h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >{{ calaulate_percentage_for_circules(App\models\Message::where(['type_message'=>0])->count(), App\models\Message::count() ) }}% </label>

                    </div>
                </a>
            </div>

             <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/mail/users') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class=" mdi mdi-account-multiple"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">المستخدمين</p>
                        <h3 class="text-success counter m-t-10" style="color:white !important">{{ App\User::count() }}</h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >100%</label>

                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/mail/new-mail') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-email"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">رسالة جديدة</p>
                        <h3 class="text-success counter m-t-10" style="color:white !important"></h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >100%</label>

                    </div>
                </a>
            </div>

            <!-- <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/orders-refused') }}">
                    <div class="widget-simple-chart text-right card-box" style="">
                        <div class="circliful-chart" data-dimension="90" data-text="58%" data-width="5" data-fontsize="14" data-percent="58" data-fgcolor="rgb(255, 253, 48)" data-bgcolor="white"></div>
                        <p class="text-muted text-nowrap m-b-10"> المرفوضة </p>
                        <h3 class="text-pink m-t-10">$ <span class="counter">12480</span></h3>
                    </div>
                </a>
            </div> -->

        </div>
        <!-- end row -->