@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('put-datatable-css')
<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  -->
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('put-datatable-script')
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  -->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection


@section('content-admin-page')
        
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> اضافة رسالة جديدة  !</h4>
                    <a class="go_back" href="{{ url('admin/dashboard') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> اضافة رسالة جديدة </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        @include('admin.pages.mail.section')<br/>

        <div class="row">  
          
             <br/>
           <!-- show if have errors -->
           @if($errors->any())
             <div class="col-lg-9 alert-danger error_alert">
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
             </div>
            @else
               <!--  show if have no errors success message -->
                @if (session('success'))
                      <div class="col-lg-12 alert-success error_alert">
                           تم ارسالة الرسالة بنجاح
                     </div>
                @endif

            @endif
          
             <div class="container_datatable col-12 col-sm-12 col-lg-12">
                <form method="Get" action="{{ url('admin/mail/filter-search') }}" >
                    <div class="col-12 col-md-5" style="display: inline-block;float:right">
                        <h4 class="m-t-0 header-title"> فلترة البحث </h4>
                        <select name="filter_type" class="custom-select mt-2" style="width:60%;float:right" required>
                            <option value="0" {{ ( ((empty($all_data) ||  ($all_data == 0) )?'selected':'') ) }} >كل المستخدمين</option>
                            <option value="1" {{ ( ((!empty($all_data) && ($all_data == 1) )?'selected':'') ) }} >المستخدمين الاكثر طلبا </option>
                            <option value="2" {{ ( ((!empty($all_data) && ($all_data == 2) )?'selected':'') ) }} >المستخدمين الاكثر طلبا لطلبات الشراء</option>
                            <option value="3" {{ ( ((!empty($all_data) && ($all_data == 3) )?'selected':'') ) }} >المستخدمين الاكثر طلبا لطلبات البحث و الشراء</option>
                            <option value="4" {{ ( ((!empty($all_data) && ($all_data == 4) )?'selected':'') ) }} >المستخدمين الاكثر طلبا لمتجر التخفيضات</option>
                            <option value="5" {{ ( ((!empty($all_data) && ($all_data == 5) )?'selected':'') ) }} >المستخدمين الجدد</option>
                            <option value="6" {{ ( ((!empty($all_data) && ($all_data == 6) )?'selected':'') ) }} >المستخدمين الاقدم</option>
                           
                            
                        </select>
                       
                        <div class="container-button-cpanel" style="padding-top: 10px;padding-left: 10px;padding-right: 10px;">
                             <button type="submit" class="btn btn-dark"> بحث </button>
                        </div>
                    </div>
                </form>
                <form method="POST" action="{{ url('admin/mail/select-users') }}">
                    @csrf
                    <div class="button-list col-md-2" style="display: inline-block;margin-top: 30px;">
                        <!-- Responsive modal -->
                        <button type="button" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal"> ارسال رسالة </button>
                    </div>
                    @include('admin.pages.mail.new-mail.formMail')
                   
                   
                    <br/><br/> <br/>
                    <table class="table table-bordered " id="laravel_datatable">
                       <thead>
                          <tr>
                             <th> تحديد </th>
                             <th> اسم المستخدم </th>
                             <th> البريد الالكترونى </th>
                          </tr>
                       </thead>
                    </table>
                </form>
            </div>
          
        </div>


@endsection
@section('script-datatable')
<script>
   jQuery(document).ready( function () {
    jQuery('#laravel_datatable').DataTable({
           processing: true,
           serverSide: false,
           ajax: "{{ url('admin/user-list-send-message/'.((!empty($all_data))?$all_data:0) ) }}",
           columns: [
                    { data: 'selectInput',name: 'selectInput'},
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                 ]
        });
     });
</script>
@endsection















    