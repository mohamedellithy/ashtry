@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('content-admin-page')

  

       <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> عرض تفاصيل الطلب !</h4>
                    
                    <a class="go_back" href="{{ url('admin/mails') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        @if(!empty($order->product_name))
                            <li class="breadcrumb-item active"> عرض تفاصيل الطلب البحث و الشراء </li>
                        @else
                             <li class="breadcrumb-item active"> عرض تفاصيل الطلب الشراء </li>
                        @endif

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="card-box m-t-20 container-messages-frame">
                    <h4 class="m-t-0 font-18"><b> رسائل الخاصة </b></h4>
                    <hr/>
                    @if(!empty($get_all_messages_for_user))
                     @foreach( $get_all_messages_for_user as $message )
                        <div class="media m-b-30">
                            @if($message->type_message == 1 )
                               <img class="d-flex ml-3 rounded-circle thumb-sm" src="{{ asset('user_image/avatar.png') }}" alt="Generic placeholder image">
                            @else
                               <img class="d-flex ml-3 rounded-circle thumb-sm" src="{{ asset('assets/img/main.png') }}" alt="Generic placeholder image">
                            @endif
                            <div class="media-body">
                                <span class="media-meta pull-right">{{ $message->created_at }}</span>
                                @if($message->type_message == 1 )
                                   <h4 class="text-primary font-16 m-0">{{ $message->user->name }}</h4>
                                   <small class="text-muted" style="font-size: 13px;">{{ $message->user->email }}</small>
                                @elseif($message->type_message == 0 )
                                   <h4 class="text-primary font-16 m-0"> اشترى </h4>
                                   <small class="text-muted" style="font-size: 13px;">المسؤل</small>
                                @endif
                                
                            </div>
                        </div>

                        <p> {{ $message->message }} </p>
                        <hr/>
                        @endforeach  
                     @endif                             
                </div>
           
                <form method="POST" action="" >
                    <div class="media m-b-0">
                        <img class="d-flex ml-3 rounded-circle thumb-sm" src="{{ asset('assets/img/main.png') }} " alt="Generic placeholder image">
                        
                        <div class="media-body">
                            <div class="card-box">
                                <div class="summernote">
                                    <h6> المسؤل الخاص بموقع اشترى</h6>
                                    <textarea class="form-control content_message"> jjj</textarea>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary waves-effect waves-light w-md m-b-30 send_message_comment"> ارسال </button>
                    </div>
                </form>
            </div>
            <div class="col-lg-4">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"> بيانات المستخدم </h4>
                    <table class="table">
                        <tbody>
                            <tr class="bg-success text-white">
                                <th scope="row">1</th>
                                <td>اسم المستخدم </td>
                                <td>{{ $user[0]->name }}</td>
                               
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>البريد الالكترونى</td>
                                <td>{{ $user[0]->email }}</td>
                               
                            </tr>
                        </tbody>
                    </table>
                </div>
            
            </div>
        </div>
        <!-- End row -->
                   

<script type="text/javascript">
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

   jQuery('.send_message_comment').click(function(e){
       e.preventDefault();
       var message_text = jQuery('.content_message').val();
       var user_id = "{{$user[0]->id}}";
       jQuery.ajax({
            type:'POST',
            datatype: 'html',
            data:{
                 message_text:message_text,
                 user_id : user_id,
            },
            url:"{{ url('admin/send-message') }}",
            success:function(data){
               jQuery('.container-messages-frame').append(data.message);
               jQuery('.content_message').val('');
            }
        });
   });
 
</script>

                

       
@endsection