@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('put-datatable-css')
<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  -->
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('put-datatable-script')
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  -->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection


@section('content-admin-page')
        
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> رسائل المستخدمين المستلمة !</h4>
                    <a class="go_back" href="{{ url('admin/dashboard') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> رسائل المستخدمين المستلمة </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        @include('admin.pages.mail.section')<br/>
        <div class="row">
           
             <div class="container_datatable col-sm-12 col-lg-12">
                <table class="table table-bordered " id="laravel_datatable">
                   <thead>
                      <tr>
                         <th> اسم المستخدم </th>
                         <th> الرسالة </th>
                         <th> نوع الرسالة </th>
                         <th> تاريخ الارسال </th>
                         <th> عرض الرسالة </th>
                         <th> حذف الرسالة </th>
                        
                      </tr>
                   </thead>
                </table>
            </div>
          
        </div>


@endsection
@section('script-datatable')
<script>
   jQuery(document).ready( function () {
    jQuery('#laravel_datatable').DataTable({
           processing: true,
           serverSide: true,
           ajax: "{{ url('admin/messages-list/recieved') }}",
           columns: [
                    { data: 'UserName', name: 'UserName' },
                    { data: 'message', name: 'message' },
                    { data: 'sending_type', name: 'sending_type' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'show', name: 'show' },
                    { data: 'delete', name: 'delete' },
                 ]
        });
     });
</script>
@endsection