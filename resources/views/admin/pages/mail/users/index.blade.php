@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('put-datatable-css')
<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  -->
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('put-datatable-script')
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  -->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection


@section('content-admin-page')
        
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> المستخدمين !</h4>
                    <a class="go_back" href="{{ url('admin/mail') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> المستخدمين </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

       @include('admin.pages.mail.section')<br/>

      <!-- show if have errors -->
       @if($errors->any())
         <div class="col-lg-9 alert-danger error_alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
         </div>
        @else
           <!--  show if have no errors success message -->
            @if (session('success'))
                  <div class="col-lg-12 alert-success error_alert"> 
                       تم حذف المستخدم بنجاح 
                 </div>
            @endif

        @endif
        <br/>
        <div class="row">
           
             <div class="container_datatable col-sm-12 col-lg-12">
                <table class="table table-bordered " id="laravel_datatable">
                   <thead>
                      <tr>
                         <th> اسم المستخدم </th>
                         <th> البريد الالكترونى </th>
                         <th> تاريخ التسجيل </th>
                         <th> التفاصيل </th>
                         <th> حذف المستخدم </th>
                         <th> التواصل  </th>
                      </tr>
                   </thead>
                </table>
            </div>
          
          
        </div>


@endsection
@section('script-datatable')
<script>
   jQuery(document).ready( function () {
       jQuery('#laravel_datatable').DataTable({
           processing: true,
           serverSide: true,
           ajax: "{{ url('admin/mail/users-list') }}",
           columns: [
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'delete', name: 'delete' },
                    { data: 'show', name: 'show' },
                    { data: 'contact', name: 'contact' },
                 ]
        });
     });
</script>
@endsection