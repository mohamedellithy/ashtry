@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('content-admin-page')
	@if(!empty($single_user))
	    @foreach( $single_user as $user )
          <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> تفاصيل بيانات المستخدم !</h4>
                    <a class="go_back" href="{{ url('admin/activites/users') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> تفاصيل بيانات المستخدم </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
	        <div class="col-lg-6">
            
                <div class="card-box">
                    <h4 class="m-t-0 header-title"> بيانات المستخدم </h4>
                    <table class="table">
                        <tbody>
                            <tr class="bg-success text-white">
                                <th scope="row">1</th>
                                <td>اسم المستخدم </td>
                                <td>{{ $user->name }}</td>
                               
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>البريد الالكترونى</td>
                                <td>{{ $user->email }}</td>
                               
                            </tr>
                            
                            
                        </tbody>
                    </table>
                </div>
            
            </div>

	        <div class="col-lg-6">
            
                <div class="card-box">
                    <h4 class="m-t-0 header-title"> طلبات المستخدم </h4>
                    <table class="table">
                        <tbody>
                        @if(!empty($user_orders))
	                        @foreach($user_orders as $order)	
	                        	<tr >
	                                <th colspan="3"><a href="{{ url('admin/order-show/'.$order->id) }}" class="btn btn-success show_order_in_single" href="#"> عرض </a></th>
	                        	</tr>
	                            <tr class="bg-info text-white">
	                                <th scope="row">1</th>
	                                <td>رقم الطلب </td>
	                                <td>#{{ $order->id }}</td>
	                               
	                            </tr>
	                            <tr class="" >
	                                <th scope="row">2</th>
	                                <td> نوع الطلب </td>
	                                <td>{{ check_type_of_order($order->product_link,$order->product_name) }}</td>
	                               
	                            </tr>
	                            
	                            <tr class="bg-warning text-white">
	                                <th scope="row">1</th>
	                                <td> تاريخ الطلب </td>
	                                <td>{{ $order->created_at }}</td>
	                               
	                            </tr>
	                            <tr class="" >
	                                <th scope="row">2</th>
	                                <td>حالة الطلب</td>
	                                <td>{{ check_status_of_order($order->order_status) }}</td>
	                               
	                            </tr>
	                        @endforeach
	                    @else
	                          <tr class="text-center"> <th colspan="3"> لايوجد طلبات للمستخد </th></tr>
                        @endif



                            
                            
                        </tbody>
                    </table>
                </div>
            
            </div>
        </div>
        <!-- end row -->

	    @endforeach

	 @endif                           
@endsection