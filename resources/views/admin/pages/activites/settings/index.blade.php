@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('content-admin-page')
       <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> ادارة صفحات الموقع !</h4>
                    <a class="go_back" href="{{ asset('admin/activites') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">اشترى </a></li>
                        <li class="breadcrumb-item active"> الرئيسية </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row" style="background-color:white !important;padding-top:20px">

            @foreach($get_all_settings as $setting)
                @if($setting->type_setting == 1)
                  <div class="col-12"> <hr/> 
                   <h5 class="card-title text-white" style="direction:rtl;text-align:right !important"> <label class="badge badge-success" style="font-size:18px !important"> الجزء الاعلى</label> </h5>
                  </div>
                @endif
                <div class="col-sm-4 col-xs-12">
                    <div class="card m-b-20 card-inverse text-white">
                        @php  $image_name = (!empty($setting->image)?$setting->image->name:'../../product_images/'.'no_image.png') @endphp

                        <img class="card-img img-fluid" src="{{ asset('settings_images/'.$image_name) }} " alt="Card image">
                        <div class="card-img-overlay" style="background-color: #211e1d5c;">
                            <h5 class="card-title text-white"> <label class="badge badge-danger" style="font-size:18px !important"> {{ $setting->title }} </label> </h5>
                            <p class="card-text"></p>
                            <p class="card-text">
                                <a class="btn btn-info" href="{{ url('admin/activites/front/edite/'.$setting->id) }} "> تعديل {{ $setting->title }} </a>
                            </p>
                        </div>
                    </div>
                </div>
                @if($setting->type_setting == 1)
                  <div class="col-12"> <hr/> 
                   <h5 class="card-title text-white" style="direction:rtl;text-align:right !important"> <label class="badge badge-success" style="font-size:18px !important"> الجزء الاسفل</label> </h5>
                  </div>
                @endif
                @if($setting->type_setting == 6)
                  <div class="col-12"> <hr/> 
                   <h5 class="card-title text-white" style="direction:rtl;text-align:right !important"> <label class="badge badge-success" style="font-size:18px !important"> محتوى الصفحات </label> </h5>
                  </div>
                @endif
            @endforeach
        

        </div>
        <!-- end row -->

@endsection
