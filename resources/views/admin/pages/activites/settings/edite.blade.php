@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection

@section('content-admin-page')
    
    @if(!empty($get_all_settings_for_page))

       @foreach($get_all_settings_for_page as $setting_page)
        
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> تعديل منتج متجر التخفيضات !</h4>
                     <a class="go_back" href="{{ url('admin/activites/settings') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> تعديل منتج متجر التخفيضات </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title">تعديل صفحة  {{ $setting_page->title }} </h4>
                    <div class="row">
                        <div class="col-12 col-md-8">
                               <!-- show if have errors -->
                               @if($errors->any())
                                 <div class="col-lg-9 alert-danger error_alert">
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                 </div>
                                @else
                                   <!--  show if have no errors success message -->
                                    @if (session('success'))
                                          <div class="col-lg-12 alert-success error_alert">
                                               تم تعديل الصحة بنجاح 
                                         </div>
                                    @endif

                                @endif
                            <div class="p-20">
                                <form method="post" action="{{ url('admin/activites/front-pages/edite/'.$setting_page->id ) }}" enctype="multipart/form-data" class="form-horizontal" role="form">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label"> عنوان الصفحة  </label>
                                        <div class="col-10">
                                            <input type="text" name="title_page" class="form-control" value=" {{ $setting_page->title }} " required>
                                        </div>
                                    </div>
                                    
                                    @if($setting_page->type_setting != 8 )
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label"> محتوى الصفحة </label>
                                            <div class="col-10">
                                                <textarea name="content_page" class="form-control" rows="5"> {{ $setting_page->content }} </textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">صورة الصفحة</label>
                                            <div class="col-10">
                                                <input type="file" name="page_image" class="form-control">
                                            </div>
                                        </div>
                                        @if($setting_page->type_setting==1)
                                             <div class="form-group row">
                                                <label class="col-2 col-form-label">صورة الصفحة</label>
                                                <div class="col-10">
                                                    <input type="file" name="page_image" class="form-control">
                                                </div>
                                            </div> 
                                        @endif

                                    @else
                                        @php  $setting = explode(",",$setting_page->content);
                                             
                                         @endphp 
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label"> حساب الانستجرام  </label>
                                            <div class="col-10">
                                                <input type="text" name="instgram_account" class="form-control" value=" {{  $setting[0] }} " required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label"> حساب تويتر  </label>
                                            <div class="col-10">
                                                <input type="text" name="twitter_account" class="form-control" value=" {{  $setting[1] }} " required>
                                            </div>
                                        </div>

                                    @endif


                                    <div class="button_container">
                                         <button type="submit" class="btn btn-primary button_add_new_product"> تعديل اعدادات الصفحة  </button>
                                    </div>


                                    
                                </form>
                            </div>
                        </div>
                        <div class="col-md-3">
                            @if(!empty($setting_page->image->name))
                             <a class="btn btn-danger" style="margin-bottom:10px" href="{{ url('admin/activites/pages/delete-image/'.$setting_page->image->id) }}"> حذف الصورة </a>
                            @endif
                             <br/>
                             <div class="col-xs-12">
                                <img class="img-single" src="{{ asset(''.(!empty($setting_page->image->name)?'settings_images/'.$setting_page->image->name:'product_images/'.'no_image.png') ) }}"/>
                             </div>
                        </div>

                    </div>
                    <!-- end row -->

                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
        <!-- end row -->
        @endforeach
    @endif

@endsection
