@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('put-datatable-css')
<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  -->
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('put-datatable-script')
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  -->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection


@section('content-admin-page')
        
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> مرحبا بك !</h4>
                     <a class="go_back" href="{{ url('admin/activites') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> الطلبات الشراء </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/activites/products') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-buffer"></i></div>
                        <p class="text-muted text-nowrap m-b-10">المنتجات</p>
                        <h3 class="text-success counter m-t-10">{{ App\models\Product::count() }}</h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >100%</label>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-9">
              <a href="{{ url('admin/actitvites/add/product') }}" class="add_new_product btn btn-info" href="#">اضافة منتج جديد</a>
            </div>
        </div>

        <div class="row">
           
             <div class="container_datatable col-sm-12 col-lg-12">
                <table class="table table-bordered " id="laravel_datatable">
                   <thead>
                      <tr>
                         <th> اسم المنتج </th>
                         <th> صورة المنتج </th>
                         <th> سعر المنتج </th>
                         <th> تخفيضات </th>
                         <th> تفاصيل المنتج </th>
                         <th> تاريخ الانشاء </th>
                         <th> تعديل المنتج </th>
                      </tr>
                   </thead>
                </table>
            </div>
          
          
        </div>


@endsection
@section('script-datatable')
<script>
   jQuery(document).ready( function () {
    jQuery('#laravel_datatable').DataTable({
           processing: true,
           serverSide: true,
           ajax: "{{ url('admin/activites/product-list') }}",
           columns: [
                    { data: 'name', name: 'name' },
                    { data: 'image', name: 'image' },
                    { data: 'price', name: 'price' },
                    { data: 'discount', name: 'discount' },
                    { data: 'details', name: 'details' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'edite', name: 'edite' },
                 ]
        });
     });
</script>
@endsection