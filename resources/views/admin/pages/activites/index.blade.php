@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('content-admin-page')
       <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> مرحبا بك !</h4>
                    <a class="go_back" href="{{ url('admin/dashboard') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">اشترى </a></li>
                        <li class="breadcrumb-item active"> الرئيسية </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
           

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/activites/products') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-buffer"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">المنتجات</p>
                        <h3 class="text-primary counter m-t-10" style="color:white !important"> {{ App\models\Product::count() }}</h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >100%</label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/activites/settings') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class=" mdi mdi-folder-outline"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important"> ادارة صفحات الموقع </p>
                        
                        <h3 class="text-primary counter m-t-10" style="color:white !important"> </h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >100%</label>
                    </div>
                </a>
            </div>

          
        </div>
        <!-- end row -->

        <!-- BAR Chart -->
        <div class="row">
          
            <div class="col-lg-12">
                <div class="portlet">
                    <!-- /primary heading -->
                    <div class="portlet-heading">
                        <h3 class="portlet-title text-dark"> تحليل بيانى للمستخدمين لعام ({{date('Y')}}) </h3>
                        <div class="portlet-widgets">
                            <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                            <span class="divider"></span>
                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default1"><i class="ion-minus-round"></i></a>
                            <span class="divider"></span>
                            <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-default1" class="panel-collapse collapse show">
                        <div class="portlet-body">
                            <div class="text-center">
                                <ul class="list-inline chart-detail-list">
                                    <li class="list-inline-item">
                                        <h5><i class="fa fa-circle m-r-5" style="color: #3bafda;"></i>عدد المستخدمين</h5>
                                    </li>
                                    <li class="list-inline-item">
                                        <h5><i class="fa fa-circle m-r-5" style="color: #dcdcdc;"></i>الشهر</h5>
                                    </li>
                                </ul>
                            </div>
                            <div id="monthlyReport" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <!-- /Portlet -->
            </div>
            <!-- col -->
        </div>
        <!-- End row-->

         <br/>        
        <!-- BAR Chart -->
        <div class="row">
          
            <div class="col-lg-12">
                <div class="container-diagrame">
                    <h3 class="text-right" style="text-align:right !important">{{ $chart1->options['chart_title'] }}</h3>
                    {!! $chart1->renderHtml() !!}
                </div>
            </div>
            <!-- col -->
        </div>
        <!-- End row-->
                

 
@endsection

@section('add-scripts')
<script src="{{asset('assets/admin/plugins/morris/morris.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/raphael/raphael-min.js')}}"></script>
<script src="{{asset('assets/admin/pages/morris.init.js')}}"></script>
<!--Morris Chart-->

 <!-- Custom main Js -->
    <script src="{{ asset('assets/admin/js/jquery.core.js') }} "></script>
    <script src="{{ asset('assets/admin/js/jquery.app.js') }} "></script>
<script> 

    new Morris.Line({
      // ID of the element in which to draw the chart.
      element: 'monthlyReport',
       parseTime: false,
      // Chart data records -- each entry in this array corresponds to a point on
      // the chart.
      data: [
        { month: '01',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-01-30 00:13:13'])->count() }}"  },
        { month: '02',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-02-30 00:13:13'])->count() }}"  },
        { month: '03',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-03-30 00:13:13'])->count() }}"  },
        { month: '04',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-04-30 00:13:13'])->count() }}"  },
        { month: '05',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-05-30 00:13:13'])->count() }}" },
        { month: '06',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-06-30 00:13:13'])->count() }}"  },
        { month: '07',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-07-30 00:13:13'])->count() }}"  },
        { month: '08',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-08-30 00:13:13'])->count() }}"  },
        { month: '09',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-09-30 00:13:13'])->count() }}"  },
        { month: '10',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-10-30 00:13:13'])->count() }}" },
        { month: '11',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-11-30 00:13:13'])->count() }}"  },
        { month: '12',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-12-30 00:13:13'])->count() }}"  },
       
        
        
      ],
      
      // The name of the data record attribute that contains x-values.
      xkey: 'month',
      // A list of names of data record attributes that contain y-values.
      ykeys: ['value'],
      // Labels for the ykeys -- will be displayed when you hover over the
      // chart.
      labels: ['مستخدم']
    });
    
     
    
    
</script>

@endsection

@section('javascript')
{!! $chart1->renderChartJsLibrary() !!}
{!! $chart1->renderJs() !!}

@endsection