@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('put-datatable-css')
<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  -->
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('put-datatable-script')
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  -->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection


@section('content-admin-page')
        
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> الطلبات متجر التخفيضات انتظار الشحن !</h4>
                    <a class="go_back" href="{{ url('admin/orders') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> الطلبات متجر التخفيضات انتظار الشحن </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        @include('admin.pages.orders.products.section')<br/>
        <div class="row">
           
             <div class="container_datatable col-sm-12 col-lg-12">
	            <table class="table table-bordered " id="laravel_datatable">
	               <thead>
	                  <tr>
	                     <th> رقم الطلب </th>
                         <th> اسم المستخدم </th>
	                     <th> اسم المنتج </th>
	                     <th> الكمية </th>
	                     <th> حالة الطلب </th>
                         <th> تاريخ الطلب </th>
                         <th> عرض الطلب </th>
	                   
	                  </tr>
	               </thead>
	            </table>
	        </div>
          
        </div>


@endsection
@section('script-datatable')
<script>
   jQuery(document).ready( function () {
    jQuery('#laravel_datatable').DataTable({
           processing: true,
           serverSide: true,
           ajax: "{{ url('admin/discount-orders-list/wait') }}",
           columns: [
                    { data: 'id',name:'id'},
                    { data: 'UserName', name: 'UserName' },
                    { data: 'product_name', name: 'product_name' },
                    { data: 'quantities', name: 'quantities' },
                    { data: 'Status', name: 'Status' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'show', name: 'show' }
                 ]
        });
     });
</script>
@endsection