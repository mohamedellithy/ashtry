@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection

@section('content-admin-page')
    
    @if(!empty($edite_single_product))

       @foreach($edite_single_product as $single_product)
        
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> تعديل منتج متجر التخفيضات !</h4>
                     <a class="go_back" href="{{ url('admin/activites/products') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> تعديل منتج متجر التخفيضات </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title">تعديل منتج </h4>
                    <div class="row">
                        <div class="col-12 col-md-8">
                               <!-- show if have errors -->
                               @if($errors->any())
                                 <div class="col-lg-9 alert-danger error_alert">
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                 </div>
                                @else
                                   <!--  show if have no errors success message -->
                                    @if (session('success'))
                                          <div class="col-lg-12 alert-success error_alert">
                                               تم تعديل المنتج بنجاح 
                                         </div>
                                    @endif

                                @endif
                            <div class="p-20">
                                <form method="post" action="{{ url('admin/activites/post-update-product/'.$single_product->id ) }}" enctype="multipart/form-data" class="form-horizontal" role="form">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label"> اسم المنتج </label>
                                        <div class="col-10">
                                            <input type="text" name="product_name" class="form-control" value=" {{ $single_product->name }} " required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label" for="example-email">سعر المنتج</label>
                                        <div class="col-10">
                                            <input type="text" id="example-email" name="product_price" class="form-control" value="{{ $single_product->price }}" placeholder="سعر المنتج">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">صورة المنتج</label>
                                        <div class="col-10">
                                            <input type="file" name="product_image" class="form-control">
                                        </div>
                                    </div>

                                   

                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">التخفيض</label>
                                        <div class="col-md-10">
                                            <input class="form-control" type="text" name="discount" value="{{ $single_product->discount }}" placeholder="التخفيض على المنتج">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">تفاصيل المنتج</label>
                                        <div class="col-10">
                                            <textarea name="description" class="form-control" rows="5"> {{ $single_product->details }} </textarea>
                                        </div>
                                    </div>
                                    <div class="button_container">
                                         <button type="submit" class="btn btn-primary button_add_new_product"> تعديل المنتج  </button>
                                    </div>


                                    
                                </form>
                            </div>
                        </div>
                        <div class="col-md-3">
                            @if(!empty($single_product->image->name))
                             <a class="btn btn-danger" style="margin-bottom:10px" href="{{ url('admin/activites/delete-image/'.$single_product->image->id) }}"> حذف الصورة </a>
                            @endif
                             <br/>
                             <div class="col-xs-12">
                                <img class="img-single" src="{{ asset('product_images/'.(!empty($single_product->image->name)?$single_product->image->name:'no_image.png') ) }}"/>
                             </div>
                        </div>

                    </div>
                    <!-- end row -->

                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
        <!-- end row -->
        @endforeach
    @endif

@endsection
