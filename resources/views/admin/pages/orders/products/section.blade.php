        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/discount-products') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-buffer"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">طلبات متجر التخفيضات</p>
                        <h3 class="text-success counter m-t-10" style="color:white !important"> {{ App\models\Purchasing::count() }} </h3>
                         <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >{{ calaulate_percentage_for_circules(App\models\Purchasing::count() , App\models\Purchasing::count() ) }} % </label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/discount-products-wait') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-truck-delivery"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">بانتظار الشحن</p>
                        <h3 class="text-primary counter m-t-10" style="color:white !important"> {{ App\models\Purchasing::where(['order_status'=>0])->count() }} </h3>
                         <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >{{ calaulate_percentage_for_circules(App\models\Purchasing::where(['order_status'=>0])->count() , App\models\Purchasing::count() ) }}% </label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/discount-products-completed') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-check-all"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important"> تم التسليم </p>
                        <h3 class="text-inverse counter m-t-10" style="color:white !important">{{ App\models\Purchasing::where(['order_status'=>1])->count() }} </h3>
                         <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >{{ calaulate_percentage_for_circules(App\models\Purchasing::where(['order_status'=>1])->count() , App\models\Purchasing::count() ) }} % </label>
                    </div>
                </a>
            </div>
              <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/activites/products') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-buffer"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">المنتجات</p>
                        <h3 class="text-primary counter m-t-10" style="color:white !important"> {{ App\models\Product::count() }}</h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        <label  class="percentage_value" >100%</label>
                    </div>
                </a>
            </div>

          

        </div>
        <!-- end row -->