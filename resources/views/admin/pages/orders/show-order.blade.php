@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('content-admin-page')
@if(!empty($single_order))
    @foreach( $single_order as $order )

       <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> عرض تفاصيل الطلب !</h4>
                    @if(empty($order->product_name))
                         <a class="go_back" href="{{ url('admin/orders-buy') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    @else
                         <a class="go_back" href="{{ url('admin/orders-search') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    @endif
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        @if(!empty($order->product_name))
                            <li class="breadcrumb-item active"> عرض تفاصيل الطلب البحث و الشراء </li>
                        @else
                             <li class="breadcrumb-item active"> عرض تفاصيل الطلب الشراء </li>
                        @endif

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
         <div class="row">
            <div class="col-12">
                <form method="post" action="{{ url('admin/orders/update-status/'.$order->id) }}" >
                   @csrf
                    <div class="card-box">
                        <div class="row">
                                <div class="col-md-9">
                                    <h4 class="m-t-0 header-title"> حالة الطلب</h4>
                                    <select name="status_update" class="custom-select mt-3" required>
                                        <!-- <option selected>Open this select menu</option> -->
                                        <option value="0" {{ ( ( !empty($order->order_status) && ($order->order_status=='0')  )?'selected':'') }} > انتظار </option>
                                        <option value="1" {{ ( ( !empty($order->order_status) && ($order->order_status=='1')  )?'selected':'') }} > موافقة على الطلب </option>
                                        <option value="2" {{ ( ( !empty($order->order_status) && ($order->order_status=='2')  )?'selected':'') }} > استلام الطلب</option>
                                        <option value="3" {{ ( ( !empty($order->order_status) && ($order->order_status=='3')  )?'selected':'') }} > رفض الطلب</option>
                                    </select>
                                   
                                </div>
                                <div class="container-button-cpanel col-md-3">
                                     <button type="submit" class="btn btn-primary"> حفظ </button>
                                </div>
                            
                        </div>
                    </div> <!-- end card-box -->
                </form>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-6">
            
                <div class="card-box">
                    <h4 class="m-t-0 header-title"> تفاصيل الطلب </h4>
                    <table class="table">
                        <tbody>
                            <tr class="table-active">
                                <th scope="row">1</th>
                                <td> رقم الطلب </td>
                                <td> #{{ $order->id }} </td>
                                
                            </tr>
                            @if(!empty($order->product_name))
                                <tr class="">
                                    <th scope="row">2</th>
                                    <td>اسم المنتج</td>
                                    <td>{{ $order->product_name }}</td>
                                    
                                </tr>
                            @else
                                <tr class="">
                                    <th scope="row">3</th>
                                    <td>رابط المنتج</td>
                                    <td>{{ $order->product_link }}</td>
                                    
                                </tr>
                            @endif
                           
                            <tr class="table-info">
                                <th scope="row">5</th>
                                <td>الكمية</td>
                                <td>{{ $order->quantity }}</td>
                               
                            </tr>
                            <tr>
                                <th scope="row">6</th>
                                <td>المقاس</td>
                                <td>{{ $order->size }}</td>
                                
                            </tr>
                            <tr class="table-warning">
                                <th scope="row">7</th>
                                <td>اللون</td>
                                <td>{{ $order->color }}</td>
                               
                            </tr>
                            <tr>
                                <th scope="row">8</th>
                                <td>حالة الطلب</td>
                                <td><span class="badge label-table badge-success"> {{ check_status_of_order($order->order_status) }}  </span> </td>
                                
                            </tr>
                            <tr class="table-danger">
                                <th scope="row">9</th>
                                <td>تاريخ انشاء الطلب</td>
                                <td>{{ $order->created_at }}</td>
                              
                            </tr>
                             <tr>
                                <th scope="row">4</th>
                                <td>ملاحظات </td>
                                <td>{{ $order->notise }}</td>
                                
                            </tr>
                            @if(!empty($order->product_name))
                             <tr>
                                <th scope="row">4</th>
                                <td> صورة المنتج </td>
                                <td>
                                   <img class="img-single" style="height: 220px;" src="{{ asset('order_images/'.(!empty($order->image->name)?$order->image->name:'no_image.png') ) }}"/>
                                </td>
                                
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
            
                <div class="card-box">
                    <h4 class="m-t-0 header-title"> بيانات المستخدم </h4>
                    <table class="table">
                        <tbody>
                            <tr class="bg-dark text-white">
                                <th scope="row">1</th>
                                <td>اسم المستخدم </td>
                                <td>{{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user->name:'') }}</td>
                               
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>البريد الالكترونى</td>
                                <td>{{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user->email:'') }}</td>
                               
                            </tr>
                            <tr class="bg-success text-white">
                                <th scope="row">3</th>
                                <td>رقم الجوال</td>
                                <td> {{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user_phone_number:'') }}
                                </td>
                               
                            </tr>
                            <tr>
                                <th scope="row">4</th>
                                <td>الاسم الاول</td>
                                <td> {{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user_first_name:'') }} </td>
                                
                            </tr>
                            <tr class="bg-info text-white">
                                <th scope="row">5</th>
                                <td>الاسم الاخير</td>
                                <td> {{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user_last_name:'') }}</td>
                               
                            </tr>
                            <tr>
                                <th scope="row">6</th>
                                <td>بلد الاقامة</td>
                                 <td>{{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user_country:'') }}
                                 </td>
                                
                            </tr>
                            <tr class="bg-warning text-white">
                                <th scope="row">7</th>
                                <td>المنطقة</td>
                                <td>{{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user_region:'') }}
                                </td>
                               
                            </tr>
                            <tr>
                                <th scope="row">8</th>
                                <td>المدينة</td>
                                <td>{{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user_city:'') }}
                                </td>
                               
                            </tr>
                            <tr class="bg-danger text-white">
                                <th scope="row">9</th>
                                <td>الرقم البريدي</td>
                                 <td>{{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user_post_code:'') }}
                                 </td>
                              
                            </tr>
                             <tr>
                                <th scope="row">9</th>
                                <td>الشارع</td>
                                <td>{{ ( ( !empty($order->shipping_info) )?$order->shipping_info->user_street:'') }}
                                </td>
                              
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            
            </div>
        </div>
        <!-- end row -->
    @endforeach
 @endif                           



                

       
@endsection