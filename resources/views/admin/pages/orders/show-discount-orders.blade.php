@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('content-admin-page')
@if(!empty($single_order))
    @foreach( $single_order as $order )

       <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> عرض تفاصيل الطلب متجر التخفيضات !</h4>
                    
                    <a class="go_back" href="{{ url('admin/discount-products') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                   
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                       
                        <li class="breadcrumb-item active"> عرض تفاصيل الطلب متجر التخفيضات </li>
                        
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
         <div class="row">
            <div class="col-12">
                <form method="post" action="{{ url('admin/orders-discount/update-status/'.$order->id) }}" >
                   @csrf
                    <div class="card-box">
                        <div class="row">
                                <div class="col-md-9">
                                    <h4 class="m-t-0 header-title"> حالة الطلب</h4>
                                    <select name="status_update" class="custom-select mt-3" required>
                                        <!-- <option selected>Open this select menu</option> -->
                                        <option value="0" {{ ( ( !empty($order->order_status) && ($order->order_status=='0')  )?'selected':'') }} > انتظار شحن الطلب </option>
                                        <option value="1" {{ ( ( !empty($order->order_status) && ($order->order_status=='1')  )?'selected':'') }} > استلام الطلب </option>
                                        
                                    </select>
                                   
                                </div>
                                <div class="container-button-cpanel col-md-3">
                                     <button type="submit" class="btn btn-primary"> حفظ </button>
                                </div>
                            
                        </div>
                    </div> <!-- end card-box -->
                </form>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-6">
            
                <div class="card-box">
                    <h4 class="m-t-0 header-title"> تفاصيل الطلب </h4>
                    <table class="table">
                        <tbody>
                            <tr class="table-active">
                                <th scope="row">1</th>
                                <td> رقم الطلب </td>
                                <td> #{{ $order->id }} </td>
                                
                            </tr>
                            
                            @php $handle_products_order = explode(',', $order->order_ids) @endphp
                            @php $handle_quantity_order = explode(',', $order->quantities) @endphp
                            @php $handle_products_orders = array_combine($handle_products_order,$handle_quantity_order) @endphp
                            
                            @foreach( $handle_products_orders as $key=>$value )
                                <tr class="">
                                    <th scope="row">2</th>
                                    <td>اسم المنتج</td>
                                    <td>{{ (!empty($order->products_name($key)->name)?$order->products_name($key)->name:'غير موجود') }}</td>
                                    
                                </tr>
                                <tr class="table-info">
                                    <th scope="row">5</th>
                                    <td>الكمية</td>
                                    <td>{{$value }}</td>
                                   
                                </tr>
                                <tr class="table-info">
                                    <th scope="row">5</th>
                                    <td>السعر</td>
                                    <td>{{ (!empty($order->products_name($key)->price)?$order->products_name($key)->price:'غير موجود') }}</td>
                                   
                                </tr>
                                <tr class="table-info">
                                    <th scope="row">5</th>
                                    <td>التخفيضات</td>
                                    <td>{{ (!empty($order->products_name($key)->discount)?$order->products_name($key)->discount:'غير موجود') }}</td>
                                   
                                </tr>
                            @endforeach
                            
                            <tr>
                                <th scope="row">8</th>
                                <td>حالة الطلب</td>
                                <td><span class="badge label-table badge-success"> {{ check_status_of_order_discount($order->order_status) }}  </span> </td>
                                
                            </tr>
                            <tr class="table-danger">
                                <th scope="row">9</th>
                                <td>تاريخ انشاء الطلب</td>
                                <td>{{ $order->created_at }}</td>
                              
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
            
                <div class="card-box">
                    <h4 class="m-t-0 header-title"> بيانات المستخدم </h4>
                    <table class="table">
                        <tbody>
                            <tr class="bg-dark text-white">
                                <th scope="row">1</th>
                                <td>اسم المستخدم </td>
                                <td>{{ $order->user->name }}</td>
                               
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>البريد الالكترونى</td>
                                <td>{{ $order->user->email }}</td>
                               
                            </tr>
                            
                            
                        </tbody>
                    </table>
                </div>
            
            </div>
        </div>
        <!-- end row -->
    @endforeach
 @endif                           



                

       
@endsection