@extends('admin.layouts.master')
@section('title')
 Dashoard
@endsection
@section('content-admin-page')
       <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"> مرحبا بك !</h4>
                    <a class="go_back" href="{{ url('admin/dashboard') }}"> <i class=" mdi mdi-arrow-right"></i> الرجوع للخلف </a>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#"> اشترى </a></li>
                        <li class="breadcrumb-item active"> الطلبات </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-4">
                <a href="{{ url('admin/orders-buy') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning text-white" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-buffer"></i></div>   
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">طلبات الشراء</p>
                        <h3 class="text-success counter m-t-10" style="color:white !important"> {{ App\models\orders::where('product_name',null)->count() }} </h3>
                        <hr style="border-top: 2px solid rgba(255, 255, 255, 1);" />
                         <label  class="percentage_value" >%{{ calaulate_percentage_for_circules(App\models\orders::where('product_name',null)->count(), App\models\orders::count() ) }} </label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-4">
                <a href="{{ url('admin/orders-search') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning text-white" >
                         <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-buffer"></i></div>   
                        <p class="text-muted text-nowrap m-b-10"  style="color:white !important">طلبات البحث و الشراء</p>
                        <h3 class="text-primary counter m-t-10"  style="color:white !important">{{ App\models\orders::where('product_link',null)->count() }}</h3>
                        <hr style="border-top: 2px solid rgba(255, 255, 255, 1);" />
                         <label  class="percentage_value" > %{{ calaulate_percentage_for_circules(App\models\orders::where('product_link',null)->count(), App\models\orders::count() ) }} </label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-4">
                <a href="{{ url('admin/discount-products') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning text-white" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-buffer"></i></div>   
                        <p class="text-muted text-nowrap m-b-10"  style="color:white !important" > طلبات متجر التخفيضات </p>
                        <h3 class="text-pink m-t-10"  style="color:white !important" ><span class="counter"> {{ App\models\Purchasing::count() }} </span></h3>
                        <hr style="border-top: 2px solid rgba(255, 255, 255, 1);" />
                         <label  class="percentage_value" >100% </label>
                    </div>
                </a>
            </div>
        </div>
        <!-- end row -->

        <!-- BAR Chart -->
        <div class="row">
          
            <div class="col-lg-12">
                <div class="container-diagrame">
                    <h3 class="text-right" style="text-align:right !important">{{ $chart1->options['chart_title'] }}</h3>
                    {!! $chart1->renderHtml() !!}
                </div>
            </div>
            <!-- col -->
        </div>
        <!-- End row-->
        <br/>        
        <!-- BAR Chart -->
        <div class="row">
          
            <div class="col-lg-12">
                <div class="container-diagrame">
                    <h3 class="text-right" style="text-align:right !important">{{ $chart2->options['chart_title'] }}</h3>
                    {!! $chart2->renderHtml() !!}
                </div>
            </div>
            <!-- col -->
        </div>
        <!-- End row-->
                

       
@endsection

@section('javascript')
{!! $chart1->renderChartJsLibrary() !!}
{!! $chart1->renderJs() !!}

{!! $chart2->renderChartJsLibrary() !!}
{!! $chart2->renderJs() !!}
@endsection