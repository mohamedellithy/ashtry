        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/orders-buy') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-buffer"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important">الطلبات</p>
                        <h3 class="text-success counter m-t-10" style="color:white !important">{{ App\models\orders::where('product_name',null)->count() }}</h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        
                        <label class="percentage_value"> {{calaulate_percentage_for_circules(App\models\orders::where(['product_name'=>null])->count() , App\models\orders::count() ) }}% </label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/orders-accepted') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                         <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-check-all"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important;">المقبولة</p>
                        <h3 class="text-primary counter m-t-10" style="color:white !important;">{{ App\models\orders::where(['product_name'=>null,'order_status'=>1])->count() }}</h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                       
                         <label class="percentage_value"> {{calaulate_percentage_for_circules(App\models\orders::where(['product_name'=>null,'order_status'=>1])->count() , App\models\orders::where(['product_name'=>null])->count() ) }} % </label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/orders-completed') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning" >
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-truck-delivery"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important;"> تم وصولها </p>
                        <h3 class="text-inverse counter m-t-10" style="color:white !important;">{{ App\models\orders::where(['product_name'=>null,'order_status'=>2])->count() }}</h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                       
                         <label class="percentage_value"> {{ calaulate_percentage_for_circules(App\models\orders::where(['product_name'=>null,'order_status'=>2])->count() , App\models\orders::where(['product_name'=>null])->count() ) }} % </label>
                    </div>
                </a>
            </div>

            <div class="col-sm-6 col-lg-3">
                <a href="{{ url('admin/orders-refused') }}">
                    <div class="widget-simple-chart text-right card-box bg-warning">
                        <div style="font-size: 40px;float: right;margin-right: 20px;color: white;"> <i class="mdi mdi-window-close"></i></div>
                        <p class="text-muted text-nowrap m-b-10" style="color:white !important;"> المرفوضة </p>
                        <h3 class="text-pink m-t-10" style="color:white !important;"><span class="counter">{{ App\models\orders::where(['product_name'=>null,'order_status'=>3])->count() }}</span></h3>
                        <hr style="width:100%;border-top: 2px solid rgba(255, 255, 255, 1);" />
                        
                        <label class="percentage_value"> {{ calaulate_percentage_for_circules(App\models\orders::where(['product_name'=>null,'order_status'=>3])->count() , App\models\orders::where(['product_name'=>null])->count() ) }}% </label>
                    </div>
                </a>
            </div>

        </div>
        <!-- end row -->