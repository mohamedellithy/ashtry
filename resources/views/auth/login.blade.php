@extends('layouts.app')

@section('content')
    <div class=" col-6 col-sm-6 col-md-4 col-lg-4 signin" id="singnInContainer">

        <div class="trying p-sm-4       " id="resetPass">
            <i id="hideReset" class="fa fa-times fa-2x text-right"></i>
            <div class="text-center">
                <h4 class="mt-4 mb-3">قم بإدخال رقم الهاتف أو البريد الالكترونى المسجل لدينا
                </h4>
                <input class="form-control" type="text" />
                <button class="btn btn-danger mt-4">إعادة تعيين كلمة السر</button>
            </div>
        </div>
        <form class="p-custom-right-left-1" method="POST" action="{{ route('login') }}">
            @csrf

            <i id="singnInBtnExit" style="color:#e64d3d" class="fa fa-times fa-2x text-right"></i>
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('البريد الالكترونى') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('كلمة المرور') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6 offset-md-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Login') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                </div>
            </div>


           <!--  <div class="form-group text-center">
                <button type="submit" class="btn btn-danger text-center red" style="font-family:cairo">تسجيل
                    الدخول</button>
                <br>
                <a class="text-center" href="#" id="openReset" style="font-family:cairo">هل
                    نسيت كلمة السر
                    ؟</a>

            </div> -->
        </form>

        <div class="divforget">
            <p class="exit">
                <i id="exitforegt" class="fa fa-times"></i>
            </p>
            <div class="form-group">
                <p class="note">
                    رقم الهاتف أو البريد الالكترونى المسجل لدينا <span>*</span>
                </p>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <div style="text-align: center;">
                    <button class="btn btn-info btn-home">إعادة تعيين كلمة المرور</button>
                </div>
            </div>
        </div>
    </div>

@endsection



























                            <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>