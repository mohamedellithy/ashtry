<!-- ------------------------------------------------------------------------------------------------ -->

<script type="text/javascript">
  jQuery(document).ready(function(){
     
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    /* here write form search and buy */
    jQuery('#searchAndBuyButton').on('click',function(e){
       e.preventDefault();
       jQuery('#container-content-page').html('<br/><br/><br/><div class="status" style="display:block !important;position:relative;"><div class="status-mes"></div></div>');
       jQuery.ajax({
	        type:'GET',
          datatype: 'html',
		    url:"{{ url('search-and-buy') }}",
		    success:function(data){
		       jQuery('#container-content-page').html(data);
		    }
        });
    });

    /* here write product discount */
    jQuery('#shoppingbtn').on('click',function(e){
       e.preventDefault();
       jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       jQuery.ajax({
	        type:'GET',
          datatype: 'html',
  		    url:"{{ url('products-discounts') }}",
  		    success:function(data){
  		       jQuery('#container-content-page').html(data);
  		    }
        });
    });

     /* here write buy */
    jQuery('#btnotlbtany').on('click',function(e){
       e.preventDefault();
       jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       jQuery.ajax({
	        type:'GET',
          datatype: 'html',
  		    url:"{{ url('buy') }}",
  		    success:function(data){
  		       jQuery('#container-content-page').html(data);
  		    }
        });
    });

   


    


  });

</script>


<!-- ------------------------------------------------------------------------------------------------ -->

<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="{{ asset('assets/js/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/modernizr-2.8.3.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.nav.js') }}"></script>
<script src="{{ asset('assets/js/meanmenu.js') }}"></script>
<script src="{{ asset('assets/js/smooth-scroll.js') }}"></script>
<script src="{{ asset('assets/owlcarousel/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.inview.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.magnific-popup.js') }}"></script>
<script src="{{ asset('assets/js/jquery.flexslider-min.js') }}"></script>
<!-- <script src="{{ asset('assets/js/scrolltopcontrol.js') }}"></script> -->
<script src="{{ asset('assets/js/slick.js') }}"></script>
<script src="{{ asset('assets/js/form-contact.js') }}"></script>
<script src="{{ asset('assets/js/aos.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>