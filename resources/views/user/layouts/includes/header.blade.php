
    <!-- Latest Bootstrap min CSS -->
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <!-- Google Font -->

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/main.png') }}">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ asset('assets/fonts/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/themify-icons.css') }}">
    <!--- owl carousel Css-->
    <link rel="stylesheet" href="{{ asset('assets/owlcarousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/owlcarousel/css/owl.theme.css') }}">
    <!--Flex slider-->
    <link rel="stylesheet" href="{{ asset('assets/css/flexslider.css') }}">
    <!--Meanmenu-->
    <link rel="stylesheet" href="{{ asset('assets/css/meanmenu.css') }}">
    <!--magnific-popup Css-->
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
    <!--Slick Css-->
    <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">
    <!--slick theme Css-->
    <link rel="stylesheet" href="{{ asset('assets/css/slick-theme.css') }}">
    <!-- component CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <!-- CSS FOR COLOR SWITCHER -->
    <link rel="stylesheet" href="{{ asset('assets/css/switcher/switcher.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/switcher/style1.css') }}" id="colors">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    <style>

    </style>
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>