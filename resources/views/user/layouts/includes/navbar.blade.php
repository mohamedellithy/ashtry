<div class="preloader">
    <div class="status">
        <div class="status-mes"></div>
    </div>
</div>
<section id="home" class="">
@if(count($all_content_page)>0)
    @foreach($all_content_page as $content_page )
        @if($content_page->type_setting ==1 )
           @php 
               $content_page_front       = $content_page->content;
               $content_page_front_image = "url('settings_images/".(!empty($content_page->image)?$content_page->image->name:'')."')";
               $content_page_front_image_small_screen = "url('".asset('assets/img/bg-vertical.png')."')";
            @endphp 
        @endif

        @if($content_page->type_setting ==5 )
           @php 
               $content_page_front_form       = $content_page->content;
               $content_page_front_image_form = "settings_images/".(!empty($content_page->image)?$content_page->image->name:'')."')";
            @endphp 
        @endif

    @endforeach
@endif
   
 

    <div class="header-head" id="header_head">
        <div class="slide_overlay">
            <div class="container-fluid">
                <div class="row">
                    @if(!Auth::user())
                        <div class="col-12 text-right">
                            <div class="hero-text p-0">
                                <h2 style="color: white;font-family:cairo" dir="rtl">{{ (!empty($content_page_front)?$content_page_front:'') }}</h2>
                                <a href="#SignUp" style="font-family:cairo" class="btn btn-default btn-home-bg" id="singnUpBtn">تسجيل
    							</a>
                                <a href="#Login" style="font-family:cairo" class="btn btn-default btn-home-bg" id="singnInBtn">تسجيل
    								دخول</a>
                                @include('user.pages.login')
                            </div>
                                @include('user.pages.registeration')
                            
                        </div>
                    @else
                        <style type="text/css">
                            @media(min-width:1000px){
                                 .header-head
                                 {
                                    //height: 250px;
                                    background-size: cover; 
                                 }
                                
                            }
                        </style>
                        <div class="col-12 text-right">
                            <div class="hero-text p-0">
                                <h2 style="color: white;font-family:cairo" dir="rtl">{{ (!empty($content_page_front)?$content_page_front:'') }}</h2>
                               
                                <div class="container-my-orders">
                                    
                                    <div class="container-orders-table" >
                                       <table class="table table-striped">
                                            <thead>
                                              <tr>
                                                <th class="th_table" colspan="3">
                                                   <a href="#" style="font-family:cairo" class="btn btn-default btn-home-bg button_title" >
                                                        متابعة الطلبات 
                                                    </a>
                                                </th>
                                                <th class="th_table">
                                                    <a href="#" style="font-family:cairo" class="btn btn-success close_button" >
                                                        x
                                                    </a>
                                                </th>
                                              </tr>
                                              <tr>
                                                <th>رقم الطلب</th>
                                                <th>تاريخ الطلب</th>
                                                <th>نوع الطلب</th>
                                                <th>حالة الطلب</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                            @php  $get_all_orders = App\models\orders::where('user_id',Auth::user()->id)->get(); @endphp
                                            @if(!empty( $get_all_orders ) )  
                                                @foreach( $get_all_orders as $my_order) 
                                                  <tr>
                                                    <td># {{ $my_order->id }}</td>
                                                    <td style="direction: ltr !important;"> {{ $my_order->created_at }} </td>
                                                    <td> {{ check_type_of_order($my_order->product_name,$my_order->product_link) }}</td>
                                                    <td> {{ check_status_of_order($my_order->order_status) }}</td>
                                                  </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <a href="#" style="font-family:cairo" class="btn btn-default btn-home-bg btn-home-myorders" >
                                        متابعة الطلبات 
                                    </a>
                                </div>
                                <div class="container-my-orders">
                                    <a style="font-family:cairo" class="btn btn-default btn-home-bg btn-home-logout" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('تسجيل الخروج') }}
                                    </a>
                                </div>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                               
                            </div>

                        </div>

                    @endif
                    <!--- END COL -->
                </div>
                <!--- END ROW -->
            </div>
            <!--- END CONTAINER -->
        </div>
        <div class="extra-added-container"></div>
        <div class="col-md-4 position-relative followReq">
            <div id="follow_request" class="request">
                <div class="head">
                    <div class="container-fluid">
                        <div class="row d-flex">

                            <div class="hide col-3 justify-content-start">
                                <button id="closefollowrequest" class="btn">
									<i class="fa fa-times fa-2x"></i>
								</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <table class="table table-bordered text-center" style="float:right;max-width:400px;">
                        <!-- <a  href="#">sadasda</a> -->
                        <thead style="background-color: #e64d3d;color: #fff;">
                            <tr>
                                <th scope="col">رقم الطلب</th>
                                <th scope="col">تاريخ الطلب </th>
                                <th scope="col">حالة الطلب</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>Thornton</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>@twitter</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- <button class="btn btn-primary requestbox  position-absolute">
				<span>متابعة</span>
				<span>الطلبات</span>
				<i class="fa fa-shopping-cart"></i>
			</button> -->
        </div>
        <!--- END slide -->
    </div>
    <!--- END slide -->


    <script type="text/javascript">
        var x = window.matchMedia("(max-width: 570px)");
        change_image_home(x);
        x.addListener(change_image_home);
        function change_image_home(x){
            if(x.matches){
                  document.getElementById("header_head").style.backgroundImage = "<?php echo $content_page_front_image_small_screen; ?> ";
                 
            }
            else
            {
                document.getElementById("header_head").style.backgroundImage = "<?php echo (!empty($content_page_front_image)?$content_page_front_image:'') ?> ";
            }
        }

       
    </script>
  

</section>
<script type="text/javascript">
  jQuery('.container-orders-table').slideUp();
  jQuery('.btn-home-myorders').click(function(){
    jQuery('.container-orders-table').slideDown();
  });
  jQuery('.close_button').click(function(){
    jQuery('.container-orders-table').slideUp();
});
</script>