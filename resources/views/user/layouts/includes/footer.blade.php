 @if(!Auth::user())
   <div class="complete-buy">
        <div class="complete-content">
            <i id="hide-complete" class="fa fa-times text-right fa-2x "></i>
            <p class="lead com-p text-center">
                الرجاء تسجيل/تسجيل الدخول لإتمام عملية الشراء
            </p>
            <div class="button-in-com text-center">
                <a href="#SignUp" style="font-family:cairo" class="btn btn-default btn-home-bg hide-complete" id="singnUpCom">تسجيل
                                </a>
                <a href="#Login" style="font-family:cairo" class="btn btn-default btn-home-bg hide-complete" id="singnInCom">تسجيل
                                        دخول</a>
            </div>
        </div>
    </div>
@endif
  <footer>
        <div class="social-media text-center">
            @if(!empty($all_content_page))
                @foreach($all_content_page as $page_privacy)
                    @if(!empty($page_privacy))
                        @if($page_privacy->type_setting==8)
                        @php  $setting = explode(',',$page_privacy->content)  @endphp 
                        <a href=" {{ $setting[1] }} "><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
                        <a href="https://maroof.sa/56853"><img src="{{ asset('assets/img/rand.svg') }}" alt="" style="width: 3.59%;margin-right: .5rem;"></a>
                        <a href="{{  $setting[0] }}"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
                        @endif
                    @endif
                @endforeach
            @endif
        </div>
        <div class="social-media text-center">
            <a href="#" style="margin-left: 2rem;" data-toggle="modal" data-target="#con-close-modal">سياسة الخصوصيه</a>
            <a href="#" data-toggle="modal" data-target="#con-close-modal_22" >سياسة الدفع والارجاع</a>
        </div>
    </footer>


    <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        @if(!empty($all_content_page))
            @foreach($all_content_page as $page_privacy)
               @if(!empty($page_privacy))
                    @if($page_privacy->type_setting==5)
                        <div class="modal-dialog">
                            <div class="modal-content" style="background-color:white">
                                <div class="modal-header">
                                    <h4 class="modal-title" style="font-family:cairo"> {{ $page_privacy->title  }} </h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12" style="padding:30px">
                                            <div class="col-md-6" style="display:inline-block; text-align:center;float:right">
                                                <h5 class="modal-title montag-footer" style="width:50%"> {{ $page_privacy->title  }} </h5>
                                                <br/>
                                               {{ $page_privacy->content  }}
                                            </div>
                                            <div class="col-md-4" style="display:inline-block; text-align:center">
                                                @if(!empty($page_privacy->image))
                                                     <img width="75%" src="{{ asset('settings_images/'.$page_privacy->image->name) }}">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    @endif
                @endif
            @endforeach
        @endif
    </div><!-- /.modal -->



    <div id="con-close-modal_22" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        @if(!empty($all_content_page))
            @foreach($all_content_page as $page_privacy)
               @if(!empty($page_privacy))
                    @if($page_privacy->type_setting==6)
                        <div class="modal-dialog">
                            <div class="modal-content" style="background-color:white">
                                <div class="modal-header">
                                    <h4 class="modal-title" style="font-family:cairo"> {{ $page_privacy->title  }} </h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12" style="padding:30px">
                                            <div class="col-md-6" style="display:inline-block; text-align:center;float:right">
                                                <h5 class="modal-title montag-footer" style="width:50%"> {{ $page_privacy->title  }} </h5>
                                                <br/>
                                               {{ $page_privacy->content  }}
                                            </div>
                                            <div class="col-md-4" style="display:inline-block; text-align:center">
                                                @if(!empty($page_privacy->image))
                                                     <img width="75%" src="{{ asset('settings_images/'.$page_privacy->image->name) }}">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    @endif
                @endif
            @endforeach
        @endif
    </div><!-- /.modal -->

   @include('user.layouts.includes.script')


</body>

</html>