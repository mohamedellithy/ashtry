<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

     <!-- SITE TITLE -->
    <title> @yield('title','user dashboard') </title>
    @include('user.layouts.includes.header')
    <meta name="csrf-token" content="{{ csrf_token() }}" />

</head>
	<body data-spy="scroll" data-offset="80">
		@include('user.layouts.includes.navbar')
        <div class="container">
            <div class="tabbable">
                <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{ ( ( !empty($active) && ($active=='sect_1') )?'active':'' ) }}" href="{{ url('buy') }}" id="searchAndBuyButton" href="#home_8" role="tab">
                            <img class="img-responsive" src="{{ asset('assets/img/00/1.png') }}">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ ( ( !empty($active) && ($active=='sect_2') )?'active':'' ) }}" href="{{ url('products-discounts') }}" id="shoppingbtn" href="#messages" role="tab">
                            <img class="img-responsive" src="{{ asset('assets/img/00/3.png') }}">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ ( ( !empty($active) && ($active=='sect_3') )?'active':'' ) }}" href="{{ url('search-and-buy') }}" id="btnotlbtany" role="tab">
                            <img class="img-responsive" src="{{ asset('assets/img/00/2.png') }}">
                        </a>
                    </li>
                   <!--  <li class="nav-item">
                        <a class="nav-link {{ ( ( !empty($active) && ($active=='sect_4') )?'active':'' ) }}" href="{{ url('show-items-cart') }}" id="cartimage" role="tab">
                            <img class="img-responsive" src="{{ asset('assets/img/00/orange_shoppictcart_1484336529.png') }}">
                        </a>
                    </li> -->
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  
                    @yield('content-page')
                </div>
            </div>
        </div>


    <div class="container-fluid" style="background:#e64d3d;position: relative">
        <div class="row">
            <div class="col-md-2"></div>

            <div class="col-md-8 col-xs-12 why">
                <div class="footer">
                    <div class="container">
                        <div class="row">
                        @if(count($all_content_page)>0)
                           @foreach($all_content_page as $content_page )

                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            @if($content_page->type_setting==2)
                                <div class="row">
                                    <div class="col-md-1"></div>

                                    <div class="col-md-5 col-image">
                                        <div class="foot-logo text-right">
                                            <img width="75%" src="{{ asset('settings_images/'.$content_page->image->name) }}">

                                        </div>
                                    </div>

                                    <div class="col-md-5 col-text-custom" style="padding-top: 90px;">
                                        <h5 class="montag-footer text-center"> {{ $content_page->title }} : </h5>
                                        <p class="p-custom" style="direction: rtl"> {{ $content_page->content }}

                                        </p>
                                    </div>


                                    <div class="col-md-1"></div>

                                </div>
                            @endif

                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            
                            @if($content_page->type_setting==3)
                                <div class="row">
                                    <div class="col-md-1"></div>

                                    <div class="col-md-5 padding00 col-text-custom col-text-custom-center " style="padding-top: 158px;">
                                        <h5 class="montag-footer text-center"> {{ $content_page->title }} : </h5>
                                        <p class="p-custom" style="direction: rtl"> {{ $content_page->content }}
                                        </p>
                                    </div>
                                    <div class="col-md-5 col-image">
                                        <div class="foot-logo text-left">
                                            <img width="100%" src="{{ asset('settings_images/'.$content_page->image->name) }}">

                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>


                                </div>
                            @endif
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            @if($content_page->type_setting==4)
                                <div class="row">
                                    <div class="col-md-1"></div>

                                    <div class="col-md-5 col-image">
                                        <div class="foot-logo text-right">
                                            <img width="100%" src="{{ asset('settings_images/'.$content_page->image->name) }}">
                                        </div>
                                    </div>
                                    <div class="col-md-5 padding00 col-text-custom" style="padding-top: 90px;">
                                        <h5 class="montag-footer text-center"> {{ $content_page->title }} : </h5>
                                        <p class="p-custom" style="direction: rtl">{{ $content_page->content }}
                                        </p>
                                    </div>
                                    <div class="col-md-1"></div>

                                </div>
                            @endif

                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->
                            <!-- ----------------------------------------------------------------------------- -->

                             @endforeach
                            @endif

                            <!--- END ROW -->
                        </div>
                        <!--- END CONTAINER -->
                    </div>

                </div>
            </div>

            <div class="col-md-2"></div>
        </div>
    </div>

		@include('user.layouts.includes.footer')
	</body>
</html>