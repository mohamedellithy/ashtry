
    <div class=" col-6 col-sm-6 col-md-4 col-lg-4 signin" id="singnInContainer">
        <div class="p-sm-4" id="resetPass" style="display:none !important">
            <form method="POST" action="{{ route('password.email') }}">
              @csrf
                    <i id="hideReset" class="fa fa-times fa-2x text-right"></i>
                    <div class="text-center">
                        <label for="email" class="col-md-10 col-form-label text-md-right label_registration">{{ __('قم بإدخال  البريد الالكترونى المسجل لدينا') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <br/>
                        <button class="btn btn-primary button_registration">إعادة تعيين كلمة السر</button>
                    </div>
            </form>
        </div>

        <form class="p-custom-right-left-1" method="POST" action="{{ route('login') }}">
            @csrf

            <i id="singnInBtnExit" style="color:#e64d3d;" class="fa fa-times fa-2x text-right"></i>
            <div class="form-group">
                <label for="email" class="col-md-4 col-form-label text-md-right label_registration">{{ __('البريد الالكترونى') }}</label>

                <div class="col-md-12 input-container">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-md-4 col-form-label text-md-right label_registration">{{ __('كلمة المرور') }}</label>

                <div class="col-md-12 input-container">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group ">
                <div class="col-md-12 offset-md-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label label_registration" for="remember">
                            {{ __('تذكرنى') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group  mb-0">
                <div class="col-md-12 offset-md-4 input-container">
                    <button type="submit" class="btn btn-primary button_registration">
                        {{ __('تسجيل الدخول') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link text-center" href="#" id="openReset" style="font-family:cairo">هل
                           {{ __('هل نسيت كلمة المرور ؟') }}    
                        </a>
                    @endif
                </div>
            </div>


           <!--  <div class="form-group text-center">
                <button type="submit" class="btn btn-danger text-center red" style="font-family:cairo">تسجيل
                    الدخول</button>
                <br>
                <a class="text-center" href="#" id="openReset" style="font-family:cairo">هل
                    نسيت كلمة السر
                    ؟</a>

            </div> -->
        </form>

        <div class="divforget">
            <p class="exit">
                <i id="exitforegt" class="fa fa-times"></i>
            </p>
            <div class="form-group">
                <p class="note">
                    رقم الهاتف أو البريد الالكترونى المسجل لدينا <span>*</span>
                </p>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <div style="text-align: center;">
                    <button class="btn btn-info btn-home">إعادة تعيين كلمة المرور</button>
                </div>
            </div>
        </div>
    </div>
