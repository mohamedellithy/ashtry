
    <div class="container" id="searchAndBuyContainer">
        <h4 class="text-center" style="font-family: Cairo;font-weight: 100;color: #484848;">
            شكرا لثقتكم واختياركم اشتري ليكون وسيطكم التجاري لتسوق الاكتروني حول العالم
        </h4>
        <div class="row">
            <div class="col-md-1"></div>
            <form action="{{ url('post-search-and-purchasing') }}" method="POST" enctype="multipart/form-data" >
             @csrf
            <div class="col-md-10" style="margin-top:30px">
                <div class="row">
                 <!--  show if have errors  -->
                   @if($errors->any())
                     <div class="col-lg-12 alert-danger error_alert">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                     </div>
                    @else
                       <!--  show if have no errors success message  -->
                        @if (session('success'))
                              <div class="col-lg-12 alert-success error_alert">
                                  تم اضافة طلبك بنجاح و سيتم الرد عليك من خلال الدعم
                             </div>
                        @endif

                    @endif
                    <div class="col-md-12 ">
                        <div id="bigbox">
                            <p class="lead" style="color:#e64d3d">بيانات المنتج</p>
                            <div class="onebox">
                                <div class="row">
                                    <div class="col-md-12 mt-2">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                                <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">أسم
        											المنتج</span>
                                            </div>
                                            <input name="product_name[]" class="form-control buy-new tx-ident-30">
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                                <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">صورة
        											المنتج</span>
                                            </div>
                                            <input name="product_image[]" type="file" class="form-control buy-new tx-ident-30" id="chosefile" aria-describedby="basic-addon3">
                                        </div>

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                                <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary lable_3_tab" id="basic-addon3">
                                                  المقاس
                                                </span>
                                            </div>
                                            <input name="product_size[]"  class="form-control buy-new tx-ident-40 input_3_tab">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="input-group ">
                                            <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                                <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary lable_3_tab" id="basic-addon3">
                                                  اللون
                                                </span>
                                            </div>
                                            <input name="product_color[]"   class="form-control buy-new tx-ident-40 input_3_tab">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                                <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary lable_3_tab" id="basic-addon3">الكمية</span>
                                            </div>
                                            <input name="product_quantity[]" type="text" class="form-control buy-new tx-ident-35 input_3_tab" id="text" aria-describedby="basic-addon3">
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                                <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">ملاحظات
        										</span>
                                            </div>
                                            <textarea name="product_notice[]" type="text"  class="form-control buy-new tx-ident-35" id="color" aria-describedby="basic-addon3"></textarea>
                                        </div>
                                    </div>
                                   
                                </div><br>
                            </div>
                        </div>
                        <div class="btn-add text-center" style="margin-top:-20px">
                            <button style="font-family:cairo" class="btn" id="addbox" onclick="event.preventDefault();">
        						<i class="fa fa-plus"></i> أضافة منتج جديد
        					</button>
                        </div>
                        <div class="btn-add" id="showclientinfo2">
                            <!-- تم حذف اid="showclientinfo2"  -->
                            <button style="font-family:cairo" class="btn btn-block open-complete"  onclick="event.preventDefault();">
        						إتمام الشراء
        					</button>
                        </div>
                        <div id="clientinfo">
                            <p class="lead" style="color:#e64d3d">الأسم و العنوان</p>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                            <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">الاسم
        										الاول</span>
                                        </div>
                                        <input name="user_first_name" class="form-control buy-new tx-ident-40 tx-ident-45">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group ">
                                        <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                            <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">الاسم
        										الاخير</span>
                                        </div>
                                        <input name="user_last_name"  class="form-control buy-new tx-ident-40 tx-ident-45">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="input-group ">
                                        <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                            <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">رقم
        										الهاتف</span>
                                        </div>
                                        <input name="user_phone_number"   class="form-control buy-new tx-ident-30 tx-ident-45">
                                    </div>
                                </div>

                            </div>

                            <br>

                            <p class="buy-head">مكان تلقي الشحنة</p>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                            <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">بلد
        										الاقامة</span>
                                        </div>
                                        <input name="user_country"   class="form-control buy-new tx-ident-40 tx-ident-45">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group ">
                                        <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                            <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">المنطقة</span>
                                        </div>
                                        <input name="user_region"   class="form-control buy-new  tx-ident-35 tx-ident-2_45">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group ">
                                        <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                            <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">المدينة</span>
                                        </div>
                                        <input name="user_city"   class="form-control buy-new  tx-ident-30 tx-ident-2_45">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group mt-3">
                                        <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                            <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">الرمز البريدي</span>
                                        </div>
                                        <input name="user_post_code"   class="form-control buy-new  tx-ident-30 tx-ident-45">
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group ">
                                        <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                            <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">الشارع</span>
                                        </div>
                                        <textarea name="user_street" class="form-control buy-new tx-ident-30 tx-ident-45" rows="3" id="comment">
                                         
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-5 mb-3 mb-md-0">
                                    <button class="btn btn-danger btn-block" id="perviousproduct" style="font-family:cairo;background-color: #e64d3d !important;border: 2px solid #e64d3d;border-radius: 10px;padding: 5px;" onclick="event.preventDefault();" >الرجوع
        								للخلف
                                    </button>
                                </div>
                                <div class="col-md-5">
                                    <button type="submit" id="payment_info" class="btn btn-danger btn-block" style="font-family:cairo;background-color: #e64d3d !important;border: 2px solid #e64d3d;border-radius: 10px;padding: 5px;" onclick="event.preventDefault();" >طريقة
        								الدفع</button>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>

                         <div id="payment_details" style="display:none">
                        <p class="lead" style="color:#e64d3d">بيانات الدفع</p>
                        <div class="divider"></div>
                        <div class="row">
                            <p class="lead" style="color:#e64d3d"> قوم بأدخال البريد الالكترونى أو رقم الهاتف اللذى ترغب بارسال فاتورة الدفع علية </p>
                            <div class="col-md-12" style="margin-bottom: 18px;">
                                <div class="input-group ">
                                    <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                        <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">
                                            البريد الالكترونى</span>
                                    </div>
                                    <input name="payment_email"  class="form-control buy-new tx-ident-40 tx-ident-4_45">
                                </div>
                            </div>
                            <br/>
                            <div class="col-md-12" style="margin-bottom: 18px;">
                                <div class="input-group ">
                                    <div class="input-group-prepend" style="position:absolute;z-index: 9999;">
                                        <span style="padding:5px;width:auto;right: -2px;position: absolute;" class="input-group-text montag btn-outline-secondary" id="basic-addon3">
                                            رقم الهاتف
                                        </span>
                                    </div>
                                    <input name="payment_phone"   class="form-control buy-new tx-ident-40 tx-ident-45">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                             <div class="col-md-1"></div>
                             <div class="col-md-5 mb-3 mb-md-0">
                                    <!-- <button class="btn btn-danger btn-block" id="perviousproduct" style="font-family:cairo;background-color: #e64d3d !important;border: 2px solid #e64d3d;border-radius: 10px;padding: 5px;" onclick="event.preventDefault();" >الرجوع
                                        للخلف</button> -->
                                   <button class="btn btn-danger btn-block " id="perviousproduct3" style="font-family:cairo;background-color: #e64d3d !important;border: 2px solid #e64d3d;border-radius: 10px;padding: 5px;" onclick="event.preventDefault();" >الرجوع
                                        للخلف
                                    </button>
                             </div>
                            <div class="col-md-5">
                                <button class="btn btn-danger btn-block"  style="font-family:cairo;background-color: #e64d3d !important;border: 2px solid #e64d3d;border-radius: 10px;padding: 5px;" >
                                    ارسال
                                </button>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                            
                               

                        
                    </div>

                    </div>
                   <!--  <div class="col-md-5">
                        <img src="assets/img/ro2ya.png" width="150%">
                    </div> -->
                </div>

             </div>
            </form>

            <div class="col-md-1"></div>

        </div>

    </div>

<script src="{{ asset('assets/js/main.js') }}"></script>