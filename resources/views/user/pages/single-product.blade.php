

    <div class="trying-two p-4" id="TryingTwo">
    	<!--  show if have errors  -->
        <a id="link_back_single" href="#messages" role="tab" class="link_back"> الرجوع <i class="fa fa-long-arrow-left fa-2x" aria-hidden="true"></i> </a>
       @if($errors->any())
         <div class="col-lg-12 alert-danger error_alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
         </div>
         <br/>
        @else
           <!--  show if have no errors success message  -->
            @if (session('success'))
                  <div class="col-lg-12 alert-success error_alert">
                      تم اضافة المنتج الى عربة التسوق
                 </div>
                 <br/>
            @endif

        @endif
        <div id="container-message-add-to-cart">
             
        </div>
       <!--  <button class="btn" id="hideTryingTwo"><i class="fa fa-times fa-2x"></i></button> -->
        <div id="imageProdect2"  class="carousel slide container-slider col-md-6 col-xs-12" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#imageProdect2" data-slide-to="0" class="active"></li>
                <li data-target="#imageProdect2" data-slide-to="1"></li>
                <li data-target="#imageProdect2" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                   @php $img_product = (!empty($show_product->image)?$show_product->image->name:'../assets/img/galaxy.png'); @endphp
                   <img class="d-block img-fluid" src="{{ asset('product_images/'.(!empty($img_product)?$img_product:'../assets/img/galaxy.png')) }}" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block img-fluid" src="{{ asset('product_images/'.(!empty($img_product)?$img_product:'../assets/img/galaxy.png')) }}" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block img-fluid" src="{{ asset('product_images/'.(!empty($img_product)?$img_product:'../assets/img/galaxy.png')) }}" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#imageProdect2" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#imageProdect2" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class=" container-single-text col-md-6 col-xs-12">
       
         
            <div class="col-12 col-sm-12 nameProduct"><h3> {{ $show_product->name }}</h3></div>
            <div class="col-12 col-sm-12 d-flex ">
                <button style="background-color:#fff;border: 1px solid #e64d3d;color:#e64d3d;" class="btn show_price" id="#add_item">{{ $show_product->price }} $</button>
            </div>
            <div class="play col-xs-12" style="padding-right:13px;">
            	<label class="label_quantity"> الكمية </label>
            	<input class="form-control buy-new " style="padding:5px;" type="number" name="quantity" />
            </div>
            <br/> 
            <div class="col-sm-4">
                <button data-value="{{ $show_product->id }}" style="background-color:#e64d3d;font-family: Cairo;border-radius: 15px;padding: 8px 8px;color:#fff;" class="btn add_to add_to_single_cart" id="#add_item"><i class="fa fa-shopping-cart"></i>اضافة الى
					السلة
				</button>
            </div>
            <div class="play col-xs-12">
                <h6> {{ $show_product->details }}  </h6>
            </div>
       

        </div>
        

        <h6 class="text-center" style="font-weight:200;color:#e64d3d;font-family:Cairo;">
            أشتري 2 وأحصل على خصم 10%</h6>

        <div class="row">
            <div class="col-sm-6">
                <div class="details">
                    <div class="row">
                        <div class="col-sm-3"><img src="{{ asset('assets/img/client-1.jpg') }}"></div>
                        <div class="col-sm-8">
                            <h6 style="margin-top:5px;font-family: Cairo;font-weight: 200">
                                حسام على</h6>
                            <span>3/5</span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <h6 style="font-family: Cairo;font-weight: 200;color:#5b5b5b">
                                هذا المنتج جميل ولكن سعره غالي</h6>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-6 mt-3 mt-sm-0">
                <div class="details">
                    <div class="row">
                        <div class="col-sm-3"><img src="{{ asset('assets/img/client-2.jpg') }}"></div>
                        <div class="col-sm-8">
                            <h6 style="margin-top:5px;font-family: Cairo;font-weight: 200">
                                مريم محمد</h6>
                            <span>5/5</span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <h6 style="font-family: Cairo;font-weight: 200;color:#5b5b5b">
                                هذا المنتج جميل جدا</h6>

                        </div>

                    </div>
                </div>
            </div>

        </div>

                <!--trying-->
    </div>
    <!---------------------------------------------------------------------------------------------------- -->
    <script type="text/javascript">
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    jQuery('.add_to_single_cart').on('click',function(e){
        e.preventDefault();
       var product_id = jQuery(this).attr('data-value');
       var quantity = jQuery('.buy-new').val();
       //jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       jQuery.ajax({
          type:'POST',
          url:"{{ url('add-to-cart') }}",
          datatype:'json',
          data:{
             id:product_id,
             quantity:quantity,
          },
          success:function(data){
            //jQuery('.num-cart').html(data);
            if(data.status=='complete'){

                jQuery('#container-message-add-to-cart').html('<div class="col-lg-12 alert-success error_alert">تم اضافة المنتج الى عربة التسوق</div><br/><br/>');
                window.location.href="#container-message-add-to-cart";
            }

          }
        });
    });


      /* here write product discount */
    jQuery('#link_back_single').on('click',function(e){
       e.preventDefault();
       jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       jQuery.ajax({
          type:'GET',
          datatype: 'html',
          url:"{{ url('products-discounts') }}",
          success:function(data){
             jQuery('#container-content-page').html(data);
          }
        });
    });

    </script>

<script src="{{ asset('assets/js/main.js') }}"></script>