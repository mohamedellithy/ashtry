
<div class="tab-pane" id="shoppingbtn" role="tabpanel"></div>
<div id="shoppingContainer">
    <section class="myproducts">
       
        <div class="container getcontainer" style="height: auto;">

            <!-- <div class="mycart position-relative" id="show_item_container" onclick="ajax_cart_item()">
                <span>سلة المشتريات</span>
                <span class="num-cart text-center ">5</span>
                <i class="fa fa-cart-plus mr-2"></i>
            </div> -->
            <div class="mycart" id="show_item_container">
                <span>سلة المشتريات</span>
                <span class="num-cart text-center "> {{ ((!empty($count_in_cart))?$count_in_cart:0) }}</span>
                <i class="fa fa-cart-plus mr-2"></i>
            </div>
            <div class="row">
                @php  $get_all_products =App\models\Product::all(); @endphp
                @foreach( $get_all_products as $product )
                    <div class="col-4">
                        <h5 style="color:#fff;font-family: Cairo;"> {{ $product->name }}</h5>
                        <div class="box">
                            <p class="nasba">
                                <span> {{ $product->discount.'%' }} </span>
                                <span class="price_front"> {{ $product->price }} </span>
                            </p>
                            @php $img_product = (!empty($product->image)?$product->image->name:'../assets/img/galaxy.png'); @endphp
                            <img src="{{ asset('product_images/'.(!empty($img_product)?$img_product:'../assets/img/galaxy.png')) }}"  class="img-responsive">

                        </div>
                        <div class="btn-box">
                            <button data-value="{{ $product->id }}"  class="btn openTrying" id="openTrying"><i class="fa fa-shopping-cart"></i>اضافة
    							الى السلة</button>
                            <a data-value="{{ $product->id }}"  href="{{ url('single-product/'.$product->id) }}"  class="btn button_details show_singel_page_details"><i class="fa fa-clipboard"></i>التفاصيل</a>
                        </div>
                    </div>
                @endforeach
              
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    /* here show items in cart*/
    jQuery('#show_item_container').click(function(e){
      e.preventDefault();
       jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       jQuery.ajax({
          type:'GET',
          url:"{{ url('show-items-cart') }}",
          success:function(data){
             jQuery('#container-content-page').html(data);
          }
        });
    });


    jQuery('.show_singel_page_details').click(function(e){
       e.preventDefault();
       var product_id = jQuery(this).attr('data-value');
       jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       jQuery.ajax({
          type:'GET',
          url:"{{ url('single-product') }}"+'/'+product_id,
          success:function(data){
             jQuery('#container-content-page').html(data);
          }
        });
    });

    jQuery('.openTrying').click(function(e){
       e.preventDefault();
       var product_id = jQuery(this).attr('data-value');
       //jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       jQuery.ajax({
          type:'POST',
          url:"{{ url('front-add-to-cart') }}",
          data:{
             id:product_id,
          },
          success:function(data){
            jQuery('.num-cart').html(data);
          }
        });
    });
    

</script>
