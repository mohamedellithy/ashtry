<div class="col-6 col-sm-6 col-md-4 col-lg-4 " style="float:right; 
background: white;
border-radius: 21px;
display: none;" id="singnUpContainer">
    <i id="singnUpBtnExit" style="color:#e64d3d" class="fa fa-times fa-2x text-right"></i>
    <form class="text-center p-custom-right-left-1" method="POST" action="{{ route('register') }}">
        @csrf

        <div class="form-group row cointainer_input_register">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('اسم المستخدم') }}</label>

            <div class="col-md-8">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row cointainer_input_register">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('البريد الاكترونى') }}</label>

            <div class="col-md-8">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row cointainer_input_register">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('كلمة المرور') }}</label>

            <div class="col-md-8">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row cointainer_input_register">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('تأكيد كلمة المرور') }}</label>

            <div class="col-md-8">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>
        <div class="form-group row cointainer_input_register mb-0">
            <div class="col-md-12 offset-md-4 text-right">
                <button type="submit" class="btn btn-primary button_registration">
                    {{ __('انشاء حساب جديد') }}
                </button>
            </div>
        </div>



        <!-- <div class="form-group row cointainer_input_register">
            <label for="inputEmail7" class="col-4 col-sm-4 col-form-label">رقم الهاتف</label>
            <div class="col-sm-8">
                <input type="email" class="form-control" id="inputEmail7">
            </div>
        </div>
        <div class="form-group row cointainer_input_register">
            <label for="inputEmail4" class="col-4 col-sm-4 col-form-label">كلمة المرور</label>
            <div class="col-sm-8">
                <input type="password" class="form-control" id="inputEmail4">
            </div>
        </div>

        <div class="form-group text-center">
            <div class=" col-sm-12">
                <button type="submit" style="font-family:cairo;background-color: #e64d3d !important;border: 2px solid #e64d3d;border-radius: 10px;" class="btn btn-danger">إنشاء حساب</button>
            </div>
        </div> -->

    </form>
</div>
