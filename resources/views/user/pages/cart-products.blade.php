
  <div class="trying p-1" id="TryingThree">
            <!-- <button class="btn" id="hideTryingThree"><i class="fa fa-times fa-2x"></i></button> -->
            <a id="link_back" href="#messages" role="tab" class="link_back"> الرجوع <i class="fa fa-long-arrow-left fa-2x" aria-hidden="true"></i> </a> </a>
            <div class="details p-1 p-sm-4" style="margin-top:0px;">
               @php $total_amount=0 @endphp 
                @if(!empty($cart_items))
                    @foreach($cart_items as $cart_item)
                       @php $total_amount+= $cart_item['quantity'] *  get_product_price($cart_item['product_id'])   @endphp
                        <div class="row container_number_trash">
                            <div class="col-2 mt-4">
                                <a data-value="{{ $cart_item['product_id'] }}" data-quantity="{{ $cart_item['quantity'] }}" href="{{ url('remove-item-cart/'.$cart_item['product_id'].'/'.$cart_item['quantity']) }}" class="add-to-trash" style="color:#e64d3d;">
                                    <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="col-3" style="margin-top:20px">
                                <h6 class="is-small" style="font-family: Cairo;font-weight:200;"> {{  get_product_name($cart_item['product_id']) }}
                                </h6>
                                <h6 style="font-family: Cairo;font-weight:200;"> {{  get_product_price($cart_item['product_id']).'$'.' * '.$cart_item['quantity'] }} </h6>
                                
                            </div>
                            <div class="col-1"></div>
                            <div class="col-5 mt-4"><img style="width:200px;" src="{{ asset('order_images/'.get_product_image($cart_item['product_id']) ) }}"></div>

                            <hr style="width:100%"/>
                        </div>
                    @endforeach
                @else
                    <h4 style="font-family: Cairo;font-weight:200;text-align:center"> لا توجد منتجات فى سلة الشراء </h4>
                @endif



              <!--   <table class="table dir_table text-center">
                    <thead>
                        <tr>

                            <th>الكمية</th>
                            <th>المقاس</th>
                            <th>اللون</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th style="color:#e64d3d" scope="row">1</th>
                            <td style="color:#e64d3d">XL</td>
                            <td style="color:#e64d3d">Black</td>
                        </tr>

                    </tbody>
                </table> -->
                <div class="container">
                    <form id="form_process_order" method="POST" action="{{ url('precess-order') }}">
                        @csrf
                        <div class="row d-flex justify-endf">
                            <div class="col-12 ">
                                <div class="container_total_amount">
                                     <h3  style="font-family: Cairo;font-size:16px" > المجموع الكلى : <span class="total_calulate"> {{ ((!empty($total_amount))?$total_amount:'0')}} </span> $ </h3>
                                </div>
                                
                                    
                                    <button type="submit" class="btn text-center add_to login_befor" id="#add_item" 
                                    @if(empty(Auth::user()->id)) onclick="event.preventDefault(); document.getElementsByClassName('complete-buy')[0].style.display='block'; " @endif > إتمام الشراء</button>
                                
                                <a href="{{ url('remove-all-items-cart') }}" class="btn text-center delet_from empty-trash" id="#delet_item">افراغ
                            سلة المشتريات <i class="fa fa-trash " aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </form>
                   
                </div>

            </div>
        </div>

 <script type="text/javascript">
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

   /* here show items in cart*/
   jQuery('.add-to-trash').click(function(e){
       e.preventDefault();
        jQuery(this).closest('.container_number_trash').fadeOut(1000);
       //jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       var product_id = jQuery(this).attr('data-value');
       var quantity   = jQuery(this).attr('data-quantity');
       var total      = Number(jQuery('.total_calulate').html());
       jQuery.ajax({
          type:'POST',
          url:"{{ url('remove-item-cart') }}"+'/'+product_id+'/'+quantity,
          data:{
             total : total,
          },
          success:function(data){
             jQuery('.total_calulate').html(data);
          }
        });
   });

    /* here show items in cart*/
   jQuery('.empty-trash').click(function(e){
       e.preventDefault();
        //jQuery(this).closest('.container_number_trash').fadeOut(1000);
       //jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       jQuery.ajax({
          type:'GET',
          url:"{{ url('remove-all-items-cart') }}",
          success:function(data){
            jQuery('.container_number_trash').fadeOut(1000);
             jQuery('.total_calulate').html(0);
          }
        });
   });


      /* here write product discount */
    jQuery('#link_back').on('click',function(e){
       e.preventDefault();
       jQuery('#container-content-page').html('<div class="status" style="display:block !important"><div class="status-mes"></div></div>');
       jQuery.ajax({
          type:'GET',
          datatype: 'html',
          url:"{{ url('products-discounts') }}",
          success:function(data){
             jQuery('#container-content-page').html(data);
          }
        });
    });

 </script>

<script src="{{ asset('assets/js/main.js') }}"></script>