 <div class="width:100%">       
    <div class="card-box">
        <h4 class="m-t-0 header-title"> تفاصيل الطلب </h4>
        <table class="table">
            <tbody>
            @foreach($orders_details as order)
                <tr class="table-active">
                    <th scope="row">1</th>
                    <td> رقم الطلب </td>
                    <td> #{{ $order->id }} </td>
                    
                </tr>
                @if(!empty($order->product_name))
                    <tr class="">
                        <th scope="row">2</th>
                        <td>اسم المنتج</td>
                        <td>{{ $order->product_name }}</td>
                        
                    </tr>
                @else
                    <tr class="">
                        <th scope="row">3</th>
                        <td>رابط المنتج</td>
                        <td>{{ $order->product_link }}</td>
                        
                    </tr>
                @endif
               
                <tr class="table-info">
                    <th scope="row">5</th>
                    <td>الكمية</td>
                    <td>{{ $order->quantity }}</td>
                   
                </tr>
                <tr>
                    <th scope="row">6</th>
                    <td>المقاس</td>
                    <td>{{ $order->size }}</td>
                    
                </tr>
                <tr class="table-warning">
                    <th scope="row">7</th>
                    <td>اللون</td>
                    <td>{{ $order->color }}</td>
                   
                </tr>
                <tr>
                    <th scope="row">8</th>
                    <td>حالة الطلب</td>
                    <td><span class="badge label-table badge-success"> {{ check_status_of_order($order->order_status) }}  </span> </td>
                    
                </tr>
                <tr class="table-danger">
                    <th scope="row">9</th>
                    <td>تاريخ انشاء الطلب</td>
                    <td>{{ $order->created_at }}</td>
                  
                </tr>
                 <tr>
                    <th scope="row">4</th>
                    <td>ملاحظات </td>
                    <td>{{ $order->notise }}</td>
                    
                </tr>
                @if(!empty($order->product_name))
                 <tr>
                    <th scope="row">4</th>
                    <td> صورة المنتج </td>
                    <td>
                       <img class="img-single" style="height: 220px;" src="{{ asset('order_images/'.(!empty($order->image->name)?$order->image->name:'no_image.png') ) }}"/>
                    </td>
                    
                </tr>
                @endif
                
            @endforeach
            </tbody>
        </table>
    </div>
</div>