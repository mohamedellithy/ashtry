<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

 Route::get('buy',function(){
 	return  view('user.pages.purchasing-orders');
 });

 Route::get('products-discounts',function(){
   	$count_items_in_cart=0;
   	if(session()->has('cart_items')){
   	   $count_items_in_cart = count(session()->get('cart_items')); 
   	}
   	return view('user.pages.orders-discount')->with('count_in_cart',$count_items_in_cart);
 });

 Route::get('search-and-buy',function(){
 	 return view('user.pages.search-and-purchasing');
 });


Route::group(['middleware'=>['auth','role:user'] ],function(){
     
     //Route::POST('post-search-and-purchasing','');
     Route::post('post-search-and-purchasing','ordersControllers@add_searching_and_products_data');
     Route::post('precess-order','productsControllers@precess_order');
   
});

  Route::get('single-product/{id}','productsControllers@show');
  Route::post('add-to-cart','productsControllers@addToCart');

  Route::get('show-items-cart','productsControllers@show_cart_items');
  Route::post('remove-item-cart/{product_id}/{quantity}','productsControllers@remove_item_cart');
  Route::get('remove-all-items-cart','productsControllers@remove_all_items_cart');
  Route::post('front-add-to-cart','productsControllers@frontAddtocart'); 









Route::get('admin',function(){
     return  view('admin.login.index');
});
Route::group(['prefix'=>'admin','as'=>'admin','middleware'=>['auth','role:admin'] ],function(){
   
    Route::get('dashboard','admin\reportsControllers@dashboard');
   // Route::get('dashboard',function(){ return  view('admin.pages.index'); });

    Route::get('orders','admin\reportsControllers@orders');
    //Route::get('orders',function(){ return  view('admin.pages.orders.index'); });
    
    /***/
    Route::get('orders-buy','admin\ordersControllers@index');
    //Route::get('orders-list/{sect}','admin\ordersControllers@orders_list');
    /***/

    /***/
    Route::get('orders-accepted',function(){  return view('admin.pages.orders.buy.accepted'); });
    /***/
    Route::get('orders-refused',function(){   return view('admin.pages.orders.buy.refused'); });
    /***/
    Route::get('orders-completed',function(){ return view('admin.pages.orders.buy.completed'); });
    /***/


     /***/
    Route::get('orders-search',function(){  return view('admin.pages.orders.buy-and-search.index'); });
    Route::get('orders-list/{type}/{sect}','admin\ordersControllers@orders_list');
    /***/

    /***/
    Route::get('orders-search-accepted',function(){  return view('admin.pages.orders.buy-and-search.accepted'); });
    /***/
    Route::get('orders-search-refused',function(){   return view('admin.pages.orders.buy-and-search.refused'); });
    /***/
    Route::get('orders-search-completed',function(){ return view('admin.pages.orders.buy-and-search.completed'); });
    /***/

    

      Route::get('discount-products',function(){  return view('admin.pages.orders.products.index'); });
      /***/
      Route::get('discount-products-wait',function(){  return view('admin.pages.orders.products.wait'); });
      /***/
      Route::get('discount-products-completed',function(){  return view('admin.pages.orders.products.completed'); });

      Route::get('discount-orders-list/{sect}','admin\productControllers@discount_orders_list');


      Route::get('activites','admin\reportsControllers@activites');
      //Route::get('activites',function(){ return view('admin.pages.activites.index'); });
      

      Route::get('mail/users',function(){ return view('admin.pages.mail.users.index'); });

      Route::get('activites/products',function(){ return view('admin.pages.orders.products.product.index'); });

      Route::get('mail/users-list','admin\userControllers@user_list');

      Route::get('activites/product-list','admin\productControllers@product_list');

      Route::get('actitvites/add/product',function(){
         return view('admin.pages.orders.products.product.add'); 
      });


      Route::post('activites/post-add-new-product','admin\productControllers@post_add_new_product');

      Route::post('orders/update-status/{order_id}','admin\ordersControllers@orders_update_status');

      Route::get('discount-orders-show/{order_id}','admin\productControllers@single_discount_orders_show');

      Route::post('orders-discount/update-status/{order_id}','admin\productControllers@orders_discount_update_status');

      Route::get('mail/user-delete/{user_id}','admin\userControllers@user_delete');
     
      Route::get('mail/user-show/{user_id}','admin\userControllers@user_single_show');
      
      Route::get('activites/product-delete/{product_id}','admin\productControllers@product_delete');

      Route::get('activites/product-edite/{product_id}','admin\productControllers@product_edite');

      Route::get('activites/delete-image/{image_id}','admin\productControllers@delete_image');
      
      Route::post('activites/post-update-product/{product_id}','admin\productControllers@post_update_product');


      Route::get('mails','admin\mailControllers@index');

      Route::get('messages-list/{sect}','admin\mailControllers@messages_list');

      Route::get('mails-recieved',function(){
        return view('admin.pages.mail.recieved'); 
      });

      Route::get('mails-send',function(){
        return view('admin.pages.mail.send'); 
      });

      Route::get('message-delete/{message_id}','admin\mailControllers@message_delete');

      Route::get('message-show/{message_id}','admin\mailControllers@message_show');

      Route::post('send-message','admin\mailControllers@send_message');

      Route::get('reports','admin\reportsControllers@show_reports');

      Route::get('activites/settings','admin\settingControllers@show_index_settings');

      Route::get('activites/front/edite/{section_id}','admin\settingControllers@front_edite_page');

      Route::post('activites/front-pages/edite/{page_id}','admin\settingControllers@post_front_edite_page');

      Route::get('activites/pages/delete-image/{image_id}','admin\settingControllers@delete_image');
      

      Route::get('mail/new-mail','admin\mailControllers@add_new_mail');

      Route::get('user-list-send-message/{filter_type}','admin\mailControllers@user_list_send_message');

      Route::get('mail/filter-search','admin\mailControllers@mail_filter_search');

      Route::post('mail/select-users','admin\mailControllers@mail_select_users');

      Route::get('refuse-order/{order_id}','admin\ordersControllers@refuse_order');

      Route::get('accept-order/{order_id}','admin\ordersControllers@accept_order');

      


      
      
    

    Route::get('order-show/{order_id}','admin\ordersControllers@order_show');

});


