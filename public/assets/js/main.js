$(document).ready(function() {
    var count = 0;
    $("#addproduct").click(function() {
        var divContents = $(".oneproduct").html();
        //console.log(divContents);
        $("#productdeiv").append(divContents);
    });
    $("#addbox").click(function() {
        if (count < 2) {
            var divContents = $(".onebox").html();
            $("#bigbox").append(divContents);
            count = count + 1;
        } else {
            alert("Maximum 3 Box Added");
        }
    });
    $("#showclientinfo").click(function() {
        $("#infoclient").slideDown("slow", function() {});
        $("#infoprod").slideUp("slow", function() {});
        $("#showclientinfo").slideUp("slow", function() {});
    });
    $(".trying").slideUp();
    $("#hideTryingTwo").click(function() {
        $("#TryingTwo").slideToggle("slow");
    });
    $("#hideTrying").click(function() {
        $("#TryingOne").slideToggle("slow");
    });
    $("#openTrying").click(function() {
        $("#TryingOne").slideToggle("slow");
    });
    $("#openTryingTwo").click(function() {
        $("#TryingTwo").slideToggle("slow");
    });
    $("#openTryingThree").click(function() {
        $("#TryingThree").slideToggle("slow");
    });
    $("#hideTryingThree").click(function() {
        $("#TryingThree").slideToggle("slow");
    });
    $("#openReset").click(function() {
        $("#resetPass").slideToggle("slow");
    });
    $("#hideReset").click(function() {
        $("#resetPass").slideToggle("slow");
    });

    $("#perviousproduct").click(function() {
        $("#bigbox").slideDown("slow", function() {});
        $("#addbox").slideDown("slow", function() {});
        $("#showclientinfo2").slideDown("slow", function() {});
        $("#clientinfo").slideUp("slow", function() {});
    });
    $("#perviousproduct2").click(function() {
        $("#infoprod").slideDown("slow", function() {});
        $("#addproduct").slideDown("slow", function() {});
        $("#showclientinfo").slideDown("slow", function() {});
        $("#infoclient").slideUp("slow", function() {});
    });
    $("#showclientinfo2").click(function() {
        $("#clientinfo").slideDown("slow", function() {});
        $("#bigbox").slideUp("slow", function() {});
        $("#showclientinfo2").slideUp("slow", function() {});
        $("#addbox").slideUp("slow", function() {});
    });
    $('.request').slideUp();
    $(".requestbox").click(function() {
        $("#singnInContainer").fadeOut();
        $("#singnUpContainer").fadeOut();
        $('.request').slideToggle(
            function() {
                $('.request').css('height', 'auto')
            }
        );
    });
    $("#closefollowrequest").click(function() {
        $('.request').slideToggle();
    });
    $(".productAdd	").click(function() {
        $(this).addClass('active').siblings().removeClass('active')
    });
    $(".dot").click(function() {
        $(this).addClass('active').siblings().removeClass('active')
    });
    $("#hide-complete").click(function() {
        $(".complete-buy").fadeOut('slow');
    });
    $(".open-complete").click(function() {
        $(".complete-buy").fadeIn('slow');
    });
    $("#singnInCom").click(function() {
        $(".complete-buy").fadeOut('slow', function() {
            $("html,body").animate({
                    scrollTop: 0
                },
                2000,
                function() {
                    $("#singnUpContainer").fadeOut(500, function() {
                        $("#singnInContainer").fadeIn();
                        $('.requestbox').fadeOut();
                        $('.tabbable').addClass('tabbable-sign-in');
                        $('.request').fadeOut();
                    });
                });
        });
    });
    $("#singnUpCom").click(function() {
        $(".complete-buy").fadeOut('slow', function() {
            $("html,body").animate({
                    scrollTop: 0
                },
                2000,
                function() {
                    $("#singnInContainer").fadeOut(500, function() {
                        $("#singnUpContainer").fadeIn();
                        $('.requestbox').fadeOut();
                        $('.tabbable').addClass('tabbable-sign-up');
                        $('.request').fadeOut();
                    });
                });
        });
    });

    //      ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    $("#searchAndBuyButton").click(function() {
        $("#searchAndBuyContainer").slideToggle();
        $("#shoppingContainer").css('display', 'none');
        $("#divotlbtany").css("display", "none");
    });
    $("#shoppingbtn").click(function() {
        $("#shoppingContainer").slideToggle();
        $("#searchAndBuyContainer").css('display', 'none');
        $("#divotlbtany").css('display', 'none');
    });
    $("#btnotlbtany").click(function() {
        $("#shoppingContainer").css('display', 'none');
        $("#searchAndBuyContainer").css('display', 'none');
        $("#divotlbtany").slideToggle();
    });
    $("#forgetpassword").click(function() {
        $(".divlogin").css('display', 'none');
        $(".divforget").css('display', 'block');
        $("#singnInContainer").css('border-radius', '21px');
    });
    $("#exitforegt").click(function() {
        $(".divlogin").css('display', 'block');
        $(".divforget").css('display', 'none');
        $("#singnInContainer").css("border-radius", "21px");
    });

});
$(document).scroll(function() {
    var y = $(this).scrollTop();
    if (y > 100) {
        $('#show_requestbox').fadeIn();
    } else {
        $('#show_requestbox').fadeOut();
    }
});
$('#payment_info').click(function(){
    $('#infoclient , #clientinfo').slideUp('slow');
    $('#payment_details').slideDown('slow');
})
$('#perviousproduct3').click(function(){
    $('#payment_details').slideUp('slow');
    $('#infoclient , #clientinfo').slideDown('slow');
});