<?php 

function get_product_name($product_id){
   $get_product_name = App\models\Product::where('id',$product_id)->pluck('name');
   return $get_product_name[0];
} 


function get_product_price($product_id){
   $get_product_price = App\models\Product::where('id',$product_id)->pluck('price');
   return $get_product_price[0];
}

function get_product_image($product_id){
   $get_product_image = App\models\Image::where('imagable_id',$product_id)->pluck('name');
   if(!empty($get_product_image[0])){
       return $get_product_image[0];
   }
   return 'mohamed.jpg';
} 

function check_type_of_order($order_link=null,$order_name=null){
	  if( !empty($order_name) && empty($order_link)  ){
         return 'طلب بحث و شراء';
	  }
	  elseif( empty($order_name) && !empty($order_link)  ){
	  	 return 'طلب شراء';
	  }

	  return 'غير معروف';

}

function check_status_of_order($status){
   if( $status==0 ){
      return 'انتظار'; 
   }
   elseif( $status==1 ){
      return 'طلب مقبول'; 
   }
   elseif( $status==2 ){
      return 'طلب تم استلامة'; 
   }
   elseif( $status==3 ){
      return 'طلب مرفوض'; 
   }
   return 0;

}

function check_status_of_order_discount($status){
  if( $status==0 ){
      return 'انتظار شحن الطلب'; 
   }
   elseif( $status==1 ){
      return 'استلام الطلب'; 
   }
 
   return 0;

}


function calaulate_percentage_for_circules($percentage,$total){
   if($total==0){
      return 0;
   }
    return intval( ( ( $percentage * 100 ) / $total ) ); 
}