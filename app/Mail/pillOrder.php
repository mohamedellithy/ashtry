<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class pillOrder extends Mailable
{
    use Queueable, SerializesModels;
    public $orders_details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order_details)
    {
         $this->orders_details = $orders_details;        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('view.user.pages.mailPillTemplate')->with('orders_details',$this->orders_details);
    }
}
