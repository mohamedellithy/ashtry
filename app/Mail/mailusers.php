<?php

namespace App\Mail;
use App\models\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class mailusers extends Mailable
{
    use Queueable, SerializesModels;
    public $alert_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($alert_info)
    {
        $this->alert_data =$alert_info;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->from('example@example.com')
                ->view('admin.pages.mail.template_email')
                ->with('alert_admin_data',$this->alert_data);
    }
}
