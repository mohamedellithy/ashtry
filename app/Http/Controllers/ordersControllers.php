<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\models\orders;
use App\models\shipping_info;
use App\Mail\pillOrder;
use Illuminate\Support\Facades\Mail;

class ordersControllers extends Controller
{
    // here write all orders process

    function handle_orders_images($image_name){
    	$process_image_dir = date('Y').'/'.date('m');
        $storageNameImage = time().'_'.$image_name->getClientOriginalName();
        $image_name->move(public_path('order_images/'.$process_image_dir),$storageNameImage); 
        return $process_image_dir.'/'.$storageNameImage;
    }
    
    function add_searching_and_products_data(Request $request){
    	$validate_args = array(
    		 'product_quantity' =>'required|array',
    		 'user_first_name'  =>'required|string',
    		 'user_last_name'   =>'required|string',
    		 'user_phone_number'=>'required|string',
    		 'user_country'     =>'required|string',
    		 'user_region'      =>'required|string',
    		 'user_city'        =>'required|string',
    		 'user_post_code'   =>'required|numeric',
    		 'user_street'      =>'required|string',
             'payment_phone'    =>'required|string',
             'payment_email'    =>'required|email',
    		);
    	if(empty($request->product_link)) {
    		$product_arg_required = array( 'product_name'=>'required|array' );
    	}
        elseif(empty($request->product_name)){
            $product_arg_required = array( 'product_link'=>'required|array' ,'product_image.*'=>'required|mimes:jpg,png,jpeg');
        }

    	$all_validate = array_merge($validate_args,$product_arg_required);
    	$this->validate($request,$all_validate); 
       
        $count_forms_fields = ( ( !empty( $request->product_name ) )?count($request->product_name):count($request->product_link));
        $ids_orders = array();
        for($number=0; $number< $count_forms_fields;$number++ ) {
            $add_new_order = new orders();
            $add_new_order->product_link     = $request->product_link[$number];
            $add_new_order->user_id          = Auth::user()->id;
            $add_new_order->product_name     = $request->product_name[$number];
            $add_new_order->notise           = $request->product_notice[$number];
            $add_new_order->quantity         = $request->product_quantity[$number];
            $add_new_order->size             = $request->product_size[$number];
            $add_new_order->color            = $request->product_color[$number];
            $add_new_order->save();
            if(!empty($request->product_name)){
                $image_process = $this->handle_orders_images( $request->file('product_image')[$number] );
                $image_insert = $add_new_order->image()->create([
                      'imagable_id'=>$add_new_order->id,
                      'imagable_type'=>'orders',
                      'name'=>$image_process,
                ]);
            }

            array_push($ids_orders, $add_new_order->id);
        }


        $shipping_info =new shipping_info(); 
        $shipping_info->user_first_name  = $request->user_first_name;
        $shipping_info->user_last_name   = $request->user_last_name;
        $shipping_info->user_id          = Auth::user()->id;
        $shipping_info->order_id         = $add_new_order->id;
        $shipping_info->user_phone_number= $request->user_phone_number;
        $shipping_info->user_country     = $request->user_country;
        $shipping_info->user_region      = $request->user_region;
        $shipping_info->user_city        = $request->user_city;
        $shipping_info->user_post_code   = $request->user_post_code;
        $shipping_info->user_street      = $request->user_street;
        $shipping_info->payment_email   = $request->payment_email;
        $shipping_info->payment_phone    = $request->payment_phone;
        $shipping_info->save();
        
        if( count($all_orders_user_selected) > 0 ){
           $all_orders_user_selected = orders::whereIn('id',$ids_orders)->get();
           Mail::to($all_users)->send(new pillOrder($all_orders_user_selected));
        }
        return back()->with('success','add good');

    	
    	
    }

    

}
