<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Redirect,Response,DB,Config;
use App\User;
use App\models\orders;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class userControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function user_list(Request $request){
        $users=DB::table('users')->get();
        return datatables()->of($users)
        ->addColumn('delete', function($row) {
               return '<a href='.url('admin/mail/user-delete/'.$row->id).' class="btn btn-danger btn-sm">حذف</a>';
            })->addColumn('show', function($row) {
               return '<a href='.url('admin/mail/user-show/'.$row->id).' class="btn btn-success btn-sm">عرض </a>';                
            })->addColumn('contact', function($row) {
               return '<a href='.url('admin/message-show/'.$row->id).' class="btn btn-info btn-sm"> التواصل </a>';                
            })->rawColumns(['show','contact','delete'])->make(true);



    }

    public function user_delete($user_id){
       User::where('id',$user_id)->delete();
       return back()->with('success','delete');   

    }

    public function user_single_show($user_id){
       $single_user = User::where('id',$user_id)->get();
       $user_orders = orders::where('user_id',$user_id)->get();
       return view('admin.pages.mail.users.single-user',compact('single_user','user_orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
