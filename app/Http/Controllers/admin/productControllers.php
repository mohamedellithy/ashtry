<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Redirect,Response,DB,Config;
use App\User;
use App\models\Product;
use App\models\Purchasing;
use App\models\Image;
use File;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class productControllers extends Controller
{

    function handle_type_of_section_order($sect){
        if($sect=='all'){
            return DB::table('purchasings')->get();
           /* return Product::all();*/
        }
        elseif($sect=='wait'){
              return DB::table('purchasings')->where('order_status',0)->select('*');
        }
        elseif($sect=='completed'){
             return DB::table('purchasings')->where('order_status',1)->select('*');
        }
    }


    function discount_orders_list(Request $request ,$sect ){
        $orders = $this->handle_type_of_section_order($sect );
        return datatables()->of($orders)
        ->addColumn('UserName', function($row) {
                return User::where('id',$row->user_id)->pluck('name')[0];
               /* return '<a href='.\URL::route('users.edit', $data->id).' class="btn btn-success btn-sm">Edit</a>
                    <a href='.\URL::route('users.show', $data->id).' class="btn btn-info btn-sm">View</a>';*/
            })->addColumn('Status', function($row) {
                if($row->order_status == 0){
                    return '<label class="badge badge-success">بانتظار الشحن</label>';
                }
                elseif($row->order_status == 1){
                    return '<label class="badge badge-danger">تم التسليم</label>';
                }
               
            })->addColumn('product_name', function($row) {
                    $product_ids_array = explode(',', $row->order_ids);
                    $products_name =DB::table('products')->whereIn('id',$product_ids_array)->pluck('name')->toArray();
                    return $products_name;
                   
                   /*return '<a href='.url('admin/order-show/'.$row->id).' class="btn btn-success btn-sm">عرض </a>';  */              
            })->addColumn('show', function($row) {
                   return '<a href='.url('admin/discount-orders-show/'.$row->id).' class="btn btn-success btn-sm">عرض </a>';                
            })->rawColumns(['UserName','product_name','Status','show'])->make(true);


    }

    function product_list(){
        $products=DB::table('products')->get();
        return datatables()->of($products)
        ->addColumn('image', function($row) {
               $img_name = DB::table('images')->where('imagable_id',$row->id)->pluck('name');
               return (!empty($img_name[0])?'<img style="width: 70px;height: 70px;" src="'.asset('product_images/'.(!empty($img_name[0])?$img_name[0]:'') ).'" />':'بدون صورة');
            })->addColumn('edite', function($row) {
               return '<a href='.url('admin/activites/product-edite/'.$row->id).' class="btn btn-info btn-sm" style="margin-bottom: 7px;" >تعديل </a>'.'<br/><a href='.url('admin/activites/product-delete/'.$row->id).' class="btn btn-danger btn-sm" style="margin-bottom: 7px;" >حذف  </a>';                
            })->rawColumns(['image','edite'])->make(true);

    }
    
    function handle_orders_images($image_name){
        $process_image_dir = date('Y').'/'.date('m');
        $storageNameImage = time().'_'.$image_name->getClientOriginalName();
        $image_name->move(public_path('product_images/'.$process_image_dir),$storageNameImage); 
        return $process_image_dir.'/'.$storageNameImage;
    }

    function post_add_new_product(Request $request){
        $this->validate($request,[
                'product_name' =>'required',
                'product_price'=>'required',
                ]);

        $product = Product::create([
            'name'    =>$request->product_name,
            'price'   =>$request->product_price,
            'discount'=>$request->discount,
            'details' =>$request->description,
        ]);
        if(!empty($request->file('product_image'))){
            $image_process = $this->handle_orders_images( $request->file('product_image'));
            $image_insert  = $product->image()->create([
                          'imagable_id'=>$product->id,
                          'imagable_type'=>'Product',
                          'name'=>$image_process,
                    ]);  
        }

        return back()->with('success','complete');

    }

    public function single_discount_orders_show($order_id){
        $single_order = Purchasing::where('id',$order_id)->get();
        return view('admin.pages.orders.show-discount-orders',compact('single_order'));
    } 

    public function orders_discount_update_status(Request $request,$order_id){
        $this->validate($request,['status_update'=>'required']);
        Purchasing::where('id',$order_id)->update(['order_status'=>$request->status_update]);
        return back();
    }

    public function product_delete($product_id){
        Product::where('id',$product_id)->delete();
        $image_name =Image::where(['imagable_id'=>$product_id,'imagable_type'=>'Product'])->pluck('name'); 
        if(count($image_name)>0){
            \File::delete(public_path("product_images/".$image_name[0]));   
            Image::where('id',$image_id)->delete();  
        }
        return back();
    }

    public function product_edite($product_id){
         $edite_single_product = Product::where('id',$product_id)->get();
         return view('admin.pages.activites.products.edite',compact('edite_single_product'));

    }

    public function delete_image($image_id){
        $image_name =Image::where('id',$image_id)->pluck('name'); 
        \File::delete(public_path("product_images/".$image_name[0]));   
        Image::where('id',$image_id)->delete();
        return back();
    }

    public function post_update_product(Request $request , $product_id){
            $this->validate($request,[
                'product_name' =>'required',
                'product_price'=>'required',
                ]);

            Product::where('id',$product_id)->update([
                                'name'     =>$request->product_name,
                                'price'    =>$request->product_price,
                                'discount' =>$request->discount,
                                'details'  =>$request->description,
                    ]);

            if(!empty($request->file('product_image'))){
                $image_process = $this->handle_orders_images( $request->file('product_image'));
                $check_count_of_images = Image::where(['imagable_id'=>$product_id,'imagable_type'=>'Product'])->count();
                if($check_count_of_images>0){
                    $image_insert = Image::where(['imagable_id'=>$product_id,'imagable_type'=>'Product'])->update([
                          'name'=>$image_process,
                    ]); 
                }
                else{
                    $image_insert = Image::create([
                          'imagable_id'=>$product_id,
                          'imagable_type'=>'Product',
                          'name'=>$image_process,
                    ]); 
                }
            }

            return back()->with('success','complete');


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
