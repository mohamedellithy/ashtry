<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\setting;
use App\models\Image;
class settingControllers extends Controller
{
   public function show_index_settings(){
      $get_all_settings = setting::orderby('created_at','asc')->get();
      return view('admin.pages.activites.settings.index',compact('get_all_settings'));
   }

    function handle_orders_images($image_name){
        $process_image_dir = date('Y').'/'.date('m');
        $storageNameImage = time().'_'.$image_name->getClientOriginalName();
        $image_name->move(public_path('settings_images/'.$process_image_dir),$storageNameImage); 
        return $process_image_dir.'/'.$storageNameImage;
    }

   public function front_edite_page($setting_page_id){
      $get_all_settings_for_page = setting::where('id',$setting_page_id)->get();
      return view('admin.pages.activites.settings.edite',compact('get_all_settings_for_page'));
   }

   public function post_front_edite_page(Request $request , $page_id){
        $content = $request->content_page;
        if($page_id==8)
        {
          $content = $request->instgram_account.','.$request->twitter_account;
        }
       
        $update_settings_page = setting::where('id',$page_id)->update(['title'=>$request->title_page,'content'=>$content]);
        if(!empty($request->file('page_image'))){
            $this->handle_delete_image(null,$page_id);
            $image_process = $this->handle_orders_images( $request->file('page_image'));
            $check_count_of_images = Image::where(['imagable_id'=>$page_id,'imagable_type'=>'setting'])->count();
            if($check_count_of_images>0){
                $image_insert = Image::where(['imagable_id'=>$page_id,'imagable_type'=>'setting'])->update([
                      'name'=>$image_process,
                ]); 
            }
            else{
                $image_insert = Image::create([
                      'imagable_id'=>$page_id,
                      'imagable_type'=>'setting',
                      'name'=>$image_process,
                ]); 
            }
        }

        return back()->with('success','complete');
   }


    public function delete_image($image_id){
        $this->handle_delete_image($image_id);
        return back();
    }

    public function handle_delete_image($image_id=null , $page_id=null){
        if(($image_id!=null) && ($page_id==null)){
            $image_name =Image::where('id',$image_id)->pluck('name'); 
        }
        elseif( ($image_id==null) && ($page_id!=null) ){
           $image_name =Image::where(['imagable_id'=>$page_id,'imagable_type'=>'setting'])->pluck('name'); 
        }

        if(!empty($image_name[0])){
            \File::delete(public_path("settings_images/".$image_name[0]));   
            Image::where('id',$image_id)->delete();
        }
            
    }
}
