<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class reportsControllers extends Controller
{
    //

    public function show_reports(){
    	/**
         *  users for months
         **/
    	$chart_options = [
	        'chart_title' => 'الزوار بالنسبة للشهر',
	        'report_type' => 'group_by_date',
	        'model' => 'App\User',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_user1 = new LaravelChart($chart_options);
        
        /**
         *  users for days
         **/
		$chart_options = [
	        'chart_title' => 'الزوار بالنسبة لليوم',
	        'report_type' => 'group_by_date',
	        'model' => 'App\User',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'day',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_period' => 'week', 
	    ];
		$chart_user_daily1 = new LaravelChart($chart_options);

		 /**
         *  users for orders
         **/
		$chart_options = [
	        'chart_title' => 'الزوار الاكثر طلبا ',
	        'report_type' => 'group_by_relationship',
	        'model' => 'App\models\orders',
	        'relationship_name' => 'user_info',
	        'group_by_field' => 'name',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_user_orders1 = new LaravelChart($chart_options);


		 /**
         *  users for orders buy
         **/
		$chart_options = [
	        'chart_title' => 'الزوار الاكثر طلبا لطلبات الشراء ',
	        'report_type' => 'group_by_relationship',
	        'model' => 'App\models\orders',
	        'new_query' => ['product_name'=>null],
	        'relationship_name' => 'user_info',
	        'group_by_field' => 'name',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_user_orders_buy1 = new LaravelChart($chart_options);


		 /**
         *  users for orders buy and search
         **/
		$chart_options = [
	        'chart_title' => 'الزوار الاكثر طلبا لطلبات البحث و الشراء ',
	        'report_type' => 'group_by_relationship',
	        'model' => 'App\models\orders',
	        'new_query' => ['product_link'=>null],
	        'relationship_name' => 'user_info',
	        'group_by_field' => 'name',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_user_orders_buy_and_search1 = new LaravelChart($chart_options);


		 /**
         *  orders for month
         **/
		$chart_options = [
	        'chart_title' => 'الطلبات بالنسبة للشهر ',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\orders',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_orders_month1 = new LaravelChart($chart_options);

		 /**
         *  orders for day
         **/
		$chart_options = [
	        'chart_title' => 'الطلبات بالنسبة لليوم ',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\orders',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'day',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_orders_day1 = new LaravelChart($chart_options);

		/**
         *  orders buy for month
         **/
		$chart_options = [
	        'chart_title' => 'الطلبات الشراء بالنسبة للشهر ',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\orders',
	        'new_query' => ['product_name'=>null],
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_orders_buy_month1 = new LaravelChart($chart_options);

		/**
         *  orders buy for day
         **/
		$chart_options = [
	        'chart_title' => 'الطلبات الشراء بالنسبة لليوم ',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\orders',
	        'new_query' => ['product_name'=>null],
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'day',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_orders_buy_day1 = new LaravelChart($chart_options);


		/**
         *  orders buy for month
         **/
		$chart_options = [
	        'chart_title' => 'الطلبات الشراء و البحث بالنسبة للشهر ',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\orders',
	        'new_query' => ['product_link'=>null],
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_orders_buy_and_search_month1 = new LaravelChart($chart_options);

		/**
         *  orders buy for day
         **/
		$chart_options = [
	        'chart_title' => 'الطلبات الشراء و البحث بالنسبة لليوم ',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\orders',
	        'new_query' => ['product_link'=>null],
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'day',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_orders_buy_and_search_day1 = new LaravelChart($chart_options);



		/**
         *  orders discount for month
         **/
		$chart_options = [
	        'chart_title' => 'الطلبات متجر التخفيضات بالنسبة للشهر ',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\Purchasing',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_orders_discount_month1 = new LaravelChart($chart_options);

		/**
         *  orders buy for day
         **/
		$chart_options = [
	        'chart_title' => 'الطلبات متجر التخفيضات بالنسبة لليوم ',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\Purchasing',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'day',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_orders_discount_day1 = new LaravelChart($chart_options);



		 /**
         *  users for orders buy and search
         **/
		$chart_options = [
	        'chart_title' => 'الزوار الاكثر طلبا لطلبات متجر التخفيضات ',
	        'report_type' => 'group_by_relationship',
	        'model' => 'App\models\Purchasing',
	        'relationship_name' => 'user',
	        'group_by_field' => 'name',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart_user_orders_discounts1 = new LaravelChart($chart_options);




		

        return view('admin.pages.reports.index',compact('chart_user1','chart_user_daily1',
        	                                            'chart_user_orders1','chart_user_orders_buy1',
        	                                            'chart_user_orders_buy_and_search1','chart_orders_month1'
        	                                            ,'chart_orders_day1','chart_orders_buy_month1',
        	                                            'chart_orders_buy_day1','chart_orders_buy_and_search_month1'
        	                                            ,'chart_orders_buy_and_search_day1','chart_orders_discount_day1',
        	                                            'chart_orders_discount_month1','chart_user_orders_discounts1'));
    }

    function dashboard(){
	    $chart_options = [
	        'chart_title' => 'الزوار بالنسبة للشهر',
	        'report_type' => 'group_by_date',
	        'model' => 'App\User',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart1 = new LaravelChart($chart_options);

		$chart_options = [
	        'chart_title' => 'طلبات بالنسبة للشهر',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\orders',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart2 = new LaravelChart($chart_options);

    	return  view('admin.pages.index',compact('chart1','chart2'));
    }

    function orders(){

    	$chart_options = [
	        'chart_title' => 'طلبات بالنسبة للشهر',
	        'report_type' => 'group_by_date',	       
	        'model'=>'App\models\orders',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart1 = new LaravelChart($chart_options);

		$chart_options = [
	        'chart_title' => 'طلبات متجر التخفيضات بالنسبة للشهر',
	        'report_type' => 'group_by_date',
	        'model' => 'App\models\Purchasing',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart2 = new LaravelChart($chart_options);
    	return  view('admin.pages.orders.index',compact('chart2','chart1'));
    }

    function activites(){

    	$chart_options = [
	        'chart_title' => 'المنتجات بالنسبة للشهر',
	        'report_type' => 'group_by_date',	       
	        'model'=>'App\models\Product',
	        'group_by_field' => 'created_at',
	        'group_by_period' => 'month',
	        'chart_type' => 'line',
	        'filter_field' => 'created_at',
	        'filter_days' => 30, // show only last 30 days
	    ];
		$chart1 = new LaravelChart($chart_options);

		return view('admin.pages.activites.index',compact('chart1'));

    }
}
