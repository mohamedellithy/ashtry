<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Redirect,Response,DB,Config;
use App\User;
use App\models\orders;
use App\models\shipping_info;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class ordersControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.pages.orders.buy.index');
    }

    function select_section_ned_data_retrive($type){
        if($type=='buy'){
          return 'product_link';
        }
        elseif($type=='buy_search'){
        return 'product_name';
        }
    }

    function handle_type_of_section_order($type,$sect){
        if($sect=='all'){
            return DB::table('orders')->where($this->select_section_ned_data_retrive($type),'!=',null)->select('*');
        }
        elseif($sect=='accepted'){
              return DB::table('orders')->where($this->select_section_ned_data_retrive($type),'!=',null)->where('order_status','1')->select('*');
        }
        elseif($sect=='completed'){
                return DB::table('orders')->where($this->select_section_ned_data_retrive($type),'!=',null)->where('order_status','2')->select('*');
        }
        elseif($sect=='refused'){
                return DB::table('orders')->where($this->select_section_ned_data_retrive($type),'!=',null)->where('order_status','3')->select('*');
        }
    }


    function orders_list(Request $request ,$type,$sect ){
        $orders = $this->handle_type_of_section_order( $type,$sect );
        return datatables()->of($orders)
        ->addColumn('UserName', function($row) {
                return User::where('id',$row->id)->pluck('name')[0];
            })->addColumn('StatusRefused', function($row) {
                if($row->order_status == '0'){
                    return '<a href='.url('admin/refuse-order/'.$row->id).' class="btn btn-success btn-sm">رفض الطلب </a>';
                }
                elseif($row->order_status == '3'){
                    return '<label class="badge badge-danger">مرفوضة</label>';
                }
            })->addColumn('StatusAccept', function($row) {
                if($row->order_status == '0'){
                    return '<a href='.url('admin/accept-order/'.$row->id).' class="btn btn-success btn-sm">قبول الطلب </a>';
                }
                elseif($row->order_status == '1'){
                    return '<label class="badge badge-danger">مقبولة</label>';
                }
                elseif($row->order_status == '2'){
                    return '<label class="badge badge-danger">تم تسليم</label>';
                }
               
            })->addColumn('show', function($row) {
                   return '<a href='.url('admin/order-show/'.$row->id).' class="btn btn-success btn-sm">عرض </a>';                
            })->rawColumns(['UserName','StatusAccept','StatusRefused','show'])->make(true);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function order_show($order_id){
        $single_order = orders::where('id',$order_id)->get();
        return view('admin.pages.orders.show-order',compact('single_order'));
    }

    public function orders_update_status(Request $request,$order_id){
        $this->validate($request,['status_update'=>'required']);
        orders::where('id',$order_id)->update(['order_status'=>$request->status_update]);
        return back();
    }

    function refuse_order($order_id){

        orders::where('id',$order_id)->update(['order_status'=>3]);
        return back();
          
    }

   
    function accept_order($order_id){
        orders::where('id',$order_id)->update(['order_status'=>1]);
        return back();
    }
}
