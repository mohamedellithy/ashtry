<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\User;
use Redirect,Response,DB,Config;
use App\models\Message;
use App\models\orders;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;
use App\Mail\mailusers;
use Illuminate\Support\Facades\Mail;
class mailControllers extends Controller
{
    public $filter;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('admin.pages.mail.index');
    }
    public function handle_type_of_section_message($sect){
        if($sect=='all'){
            return DB::table('messages')->get();
        }
        elseif($sect=='recieved'){
              return DB::table('messages')->where('type_message',1)->select('*');
        }
        elseif($sect=='send'){
                return DB::table('messages')->where('type_message',0)->select('*');
        }
       /* elseif($sect=='refused'){
                return DB::table('orders')->where($this->select_section_ned_data_retrive($type),'!=',null)->where('order_status','3')->select('*');
        }*/
    }

    public function messages_list(Request $request,$sect){
        $messages = $this->handle_type_of_section_message( $sect );
        return datatables()->of($messages)
        ->addColumn('UserName', function($row) {
                return User::where('id',$row->user_id)->pluck('name')[0];
               /* return '<a href='.\URL::route('users.edit', $data->id).' class="btn btn-success btn-sm">Edit</a>
                    <a href='.\URL::route('users.show', $data->id).' class="btn btn-info btn-sm">View</a>';*/
            })->addColumn('sending_type', function($row) {
                if($row->type_message == '0'){
                    return '<label class="badge badge-info"> مرسلة </label>';
                }
                elseif($row->type_message == '1'){
                    return '<label class="badge badge-danger"> مستلمة </label>';
                }
            })->addColumn('show', function($row) {
                   return '<a href='.url('admin/message-show/'.$row->user_id).' class="btn btn-success btn-sm"> عرض </a>';                
            })->addColumn('delete', function($row) {
                   return '<a href='.url('admin/message-delete/'.$row->id).' class="btn btn-danger btn-sm"> حذف الرسالة </a>';                
            })->rawColumns(['UserName','sending_type','show','delete'])->make(true);

    }

    public function message_delete($message_id){
        Message::where('id',$message_id)->delete();
        return back();
    }

    public function message_show($user_id){
        /*$get_user_messages = Message::where('id',$message_id)->pluck('user_id');
        $user_id = $get_user_messages[0];*/

        $get_all_messages_for_user = Message::where('user_id',$user_id)->orderby('created_at','asc')->get();
        $user=User::where('id',$user_id)->get();
        return view('admin.pages.mail.single-message',compact('get_all_messages_for_user','user'));
    }

    function send_message(Request $request){
         //$request->user_id;
        $insert_message = Message::create(['user_id'=>$request->user_id,'message'=>$request->message_text,'type_message'=>0]);
        $view_html =   '<div class="media m-b-30">
                            <img class="d-flex ml-3 rounded-circle thumb-sm" src="'.asset('assets/img/main.png').'" alt="Generic placeholder image">
                            <div class="media-body">
                                <span class="media-meta pull-right">'.$insert_message->created_at.'</span>
                                   <h4 class="text-primary font-16 m-0"> اشترى </h4>
                                   <small class="text-muted" style="font-size: 13px;">المسؤل</small>
                            </div>
                        </div>
                        <p> '.$insert_message->message.'</p>
                        <hr/>';

        $alert_data = Message::findOrFail($insert_message->id);
        // $alert_data = Message::findOrFail($request->message_tex);
        $get_user = User::find($request->user_id); 
        Mail::to($get_user)->send(new mailusers($request->message_tex));
        return response()->json(['message'=>$view_html]);
    }

  

   function add_new_mail(){
      //$users_orders = DB::table('users')->select('orders.id', DB::raw('count(*) as count_orders'))->orderby('count_orders','desc')->get(); 
      //$users_orders = User::withCount('orders as count_orders')->orderby('count_orders','desc')->get(); 
      return view('admin.pages.mail.new-mail.index');
   }

   function user_list_send_message(Request $request ,$filter_type){
      
        if( ($filter_type == 0 ) || empty( $filter_type) ){
              $users = User::all();  
        }
        elseif(($filter_type == 1 )){
           $users = User::withCount('orders as count_orders')->orderby('count_orders','desc')->get();  
        }
        elseif(($filter_type == 2 ) ){
          $users = User::leftJoin('orders', 'orders.user_id',"=", 'users.id')
          ->where('orders.product_name',null)->where('orders.product_link','!=',null)
          ->withCount('orders as count_orders')->orderby('count_orders','desc')->get();
        }
        elseif(($filter_type == 3 )){
           $users = User::leftJoin('orders', 'orders.user_id',"=", 'users.id')
          ->where('orders.product_link',null)->where('orders.product_name','!=',null)
          ->withCount('orders as count_orders')->orderby('count_orders','desc')->get(); 
        }
        elseif(($filter_type == 4 )){
           $users = User::withCount('Purchasing as count_orders')->orderby('count_orders','desc')->get();  
        }
        elseif(($filter_type == 5 )){
          $users = User::orderby('created_at','desc')->get(); 
        }
        elseif(($filter_type == 6 )){
          $users = User::orderby('created_at','asc')->get(); 
        }
        return datatables()->of($users)
        ->addColumn('selectInput', function($row) {
               return   '<div class="checkbox checkbox-danger">
                            <input id="checkbox'.$row->id.'" type="checkbox" value="'.$row->id.'" name="selectUser[]">
                            <label for="checkbox'.$row->id.'"></label>
                        </div>';

            })->rawColumns(['selectInput'])->make(true);

   }


   function mail_filter_search(Request $request){
         $filter_type = $request->filter_type;
         return view('admin.pages.mail.new-mail.index')->with('all_data',$filter_type);


   }
    
   function mail_select_users(Request $request){
        $all_users = User::whereIn('id',$request->selectUser)->get(); 
        $alert_data = $request->message_alert;
        Mail::to($all_users)->send(new mailusers($alert_data));
        return back()->with('success','complete');
        
   }

   

}
