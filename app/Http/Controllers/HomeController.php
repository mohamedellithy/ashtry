<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Image;
use App\models\setting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $all_content_page = setting::all();
        return view('index',compact('all_content_page'));
    }
}
