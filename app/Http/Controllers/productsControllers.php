<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Product;
use App\models\Cart;
use Auth;
use Session;
use App\models\Purchasing;

class productsControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //

    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    function addToCart(Request $request){
         /*$addToCar = Cart::create([
              'product_id'=>$id,
              'user_id'=>Auth::user()->id,
              'quantity'=>$request->quantity,
            ]);*/
        $get_container_of_cart = array();
        if(session()->has('cart_items')){
           $get_container_of_cart = Session::get('cart_items');   
        }
            $get_container_of_cart [] = array('product_id'=>$request->id,
                                             'user_id'=>(!empty(Auth::user()->id)?Auth::user()->id:''),
                                             'quantity'=>$request->quantity
                                       );
       // $add_new_cart_items  = array_merge($get_container_of_cart,$new_items_for_cart);


        // Store a piece of data in the session...
        Session::put('cart_items',$get_container_of_cart);

         return response()->json(['status'=>'complete']);
    }

    function frontAddtocart(Request $request){

        $get_container_of_cart = array();
        if(session()->has('cart_items')){
           $get_container_of_cart = Session::get('cart_items');   
        }
            $get_container_of_cart [] = array('product_id'=>$request->id,
                                             'user_id'=>(!empty(Auth::user()->id)?Auth::user()->id:''),
                                             'quantity'=>1
                                    );
       // $add_new_cart_items  = array_merge($get_container_of_cart,$new_items_for_cart);


        // Store a piece of data in the session...
        Session::put('cart_items',$get_container_of_cart);
        if(session()->has('cart_items')){
            return count( Session::get('cart_items') );
        }
        return 0;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         //session()->flush();
       return view('user.pages.single-product')->with('show_product',Product::find($id));
    }

    function show_cart_items(){
       return view('user.pages.cart-products')->with('cart_items',session()->get('cart_items') );   
    }

    function remove_item_cart(Request $request,$product_id,$quantity){
        $all_items_cart = Session::get('cart_items');
        foreach ($all_items_cart as $key => $arr) {
           if( ( $arr["product_id"] == $product_id ) && ($arr['quantity'] == $quantity)){
                unset($all_items_cart[$key]);
                break;
           }
        }
        Session::put('cart_items',$all_items_cart);
        $total_amount = $quantity *  get_product_price($product_id);

        return $request->total - $total_amount;
    }

    function remove_all_items_cart(){
        session()->flush();
        return back();
    }

    function precess_order(Request $request){
        $all_items_cart = Session::get('cart_items');
        $orders_id=array();
        $Quantities=array();
        foreach ($all_items_cart as $key => $arr) {
            array_push($orders_id,$arr["product_id"]);
            array_push($Quantities,$arr["quantity"]);
          
        }

        $addToPurchasing = Purchasing::create([
              'order_ids'=>implode(',',$orders_id),
              'user_id'=>Auth::user()->id,
              'quantities'=>implode(',',$Quantities),
            ]);
        session()->forget('cart_items');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
