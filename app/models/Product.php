<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable=[
        'name','price','discount','details',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imagable');
    }
}
