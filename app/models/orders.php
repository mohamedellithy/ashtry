<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\models\shipping_info;

class orders extends Model
{
    //
    protected $fillable=[
        'product_name','product_link','notise','quantity','size','color'
    ];

    
    public function image()
    {
        return $this->morphOne(Image::class, 'imagable');
    }
    
    public function user_info(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function shipping_info(){
        return $this->hasOne(shipping_info::class, 'order_id');
    }
}
