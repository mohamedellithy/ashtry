<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class setting extends Model
{
    //

    protected $fillable=[
        'type_setting','title','content',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imagable');
    }
}
