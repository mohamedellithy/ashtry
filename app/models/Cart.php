<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\models\Product;
use App\User;

class Cart extends Model
{
    //
    protected $fillable = [
      'quantity','product_id','user_id'
    ];

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
