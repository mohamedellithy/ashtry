<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    "orders"  => orders::class,
    "Product" => Product::class,
    "setting" => setting::class,
]);
class Image extends Model
{

    protected $fillable = [
        'imagable_id','imagable_type','name','img_url'
    ];

    public function imagable()
    {
        return $this->morphTo();
    }
}
