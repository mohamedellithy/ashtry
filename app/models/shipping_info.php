<?php

namespace App\models;
use App\User;
use App\models\orders;
use Illuminate\Database\Eloquent\Model;

class shipping_info extends Model
{
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order(){
        return $this->belongsTo(orders::class, 'order_id');
    }
}
