<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Purchasing extends Model
{
    //

    protected $fillable=[
        'order_ids','user_id','quantities','order_status',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function products_name($products_id){
        $get_product_name = Product::where('id',$products_id)->select('name','price','discount')->get();
        if(count($get_product_name)>0){
           return $get_product_name[0];
        }
        
    }

}
