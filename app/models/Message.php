<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Message extends Model
{
    //
    protected $fillable=[
        'user_id','message','type_message','read_notify'
    ];


    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }


}
